/*6*/

create view vw_total_medicos_sin_especialidades as (
    select m.sexo as Sexo, count (m.sexo) as Cantidad
        from medicos as m left join espemedi as e on m.matricula = e.matricula
        where e.matricula is null
        group by  m.sexo )
