/*18*/

/*

Crear un Trigger que controle que ningún paciente se haga un estudio si tiene
impago un estudio del mismo tipo (excepto que se encuentre cubierto en un 100%
por la obra social).

*/

CREATE TRIGGER control_Estudios_historias ON historias FOR INSERT AS
	declare @dni int
	declare @estudio int
	declare @ooss nvarchar(50)
	declare @plan int
	declare @cobertura int
	declare @dni_aux int
	SELECT @estudio = idestudio, @dni = dni, @ooss = sigla
		FROM inserted
	SELECT @plan = a.nroplan
		FROM afiliados a
		WHERE (a.dni = @dni) AND (a.sigla = @ooss)
	SELECT
		@cobertura = c.cobertura
	FROM coberturas c
	WHERE (c.sigla = @OOSS) AND
				(c.idestudio = @estudio)AND
				(c.nroplan = @plan)

	if (@cobertura <> 100)
	begin
		SELECT @dni_aux = h.dni
			FROM historias h
		WHERE (h.dni = @dni) AND
					(h.pagado = 0) AND
					(h.fecha <= GETDATE())
		if (@dni_aux IS NOT NULL)
		begin
			DELETE FROM historias WHERE EXISTS ( SELECT * FROM inserted)
		end
	end

	else
		begin
			print 'Paciente DNI Numero ' + @dni + ' tiene estudios sin abonar'
		end
