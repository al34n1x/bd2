/*14*/

/*

Crear los Triggers que determinen el número correspondiente al id de especialidad
y al id de estudio cada vez que se registre una nueva especialidad o un nuevo estudio
en la Base de Datos. Los id no deben ingresarse sino calcularse como el número siguiente
correlativo a los ya existentes para cada caso.

*/

/*Por especialidad*/
CREATE TRIGGER idespec ON especialidades FOR INSERT AS
	declare @id int
	SELECT
		@id = MAX(idespecialidad)
	FROM especialidades
	GROUP BY idespecialidad
	print 'especialidad numero ' + CAST(@id AS varchar) + ' se ha agregada a la tabla especialidades'

/*Por estudio*/
CREATE TRIGGER idest ON estudios FOR INSERT AS
		declare @id int
		SELECT
			@id = MAX(idestudio)
			FROM estudios
			GROUP BY idestudio
		print 'estudio numero ' + CAST(@id AS varchar) + 'se ha agregado a la tabla estudios'
