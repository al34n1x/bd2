/*
3.	Función escalar y T-SQL.
Crear una función FA999999 que reciba una cadena de caracteres y devuelve la misma cadena de caracteres escrita en minúsculas,
capitalizando cada palabra. Por ejemplo si ingreso la cadena "QUE FACIL ESTE EXAMEN" me debería devolver la cadena "Que Facil Este Examen"
*/

CREATE FUNCTION FA999999(@stringInicial varchar (255))
  returns varchar(255)
  AS
    begin

      declare @stringConvertido varchar (255)
      declare @i int
	  select @stringConvertido = ''
      select @i = 1
      while (@i <= len(@stringInicial))

	  begin
        if((@i=1) or (substring(@stringInicial,@i-1,1)=' '))
        begin
          select @stringConvertido = (@stringConvertido + upper(substring(@stringInicial,@i,1)))
        end

	    else
        begin
          select @stringConvertido = (@stringConvertido + lower(substring(@stringInicial,@i,1)))
        end

        select @i = @i + 1
      end
      return @stringConvertido
    end

select dbo.FA999999('QUE FACIL ESTE EXAMEN') as 'String Formateado'
