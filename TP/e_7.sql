/*7*/
/* Cuál es el paciente que tiene más estudios recetados según su cobertura
*/
SELECT DISTINCT o.nombre, pp.nroplan, p.dni as DNI, p.nombre as Nombre, p.apellido as Apellido, p.sexo as Sexo, p.nacimiento as FechaNac
FROM
	pacientes p, afiliados a, OOSS o, planes pp, coberturas c
WHERE EXISTS(
							SELECT temp.dni, temp.cantidad
								FROM (
											SELECT historias.dni, COUNT(historias.dni) AS 'cantidad'
												FROM historias
												GROUP BY historias.dni
										 ) AS temp
							 WHERE temp.cantidad = (
																				SELECT MAX(temp2.cantidad)
																					FROM (
																									SELECT historias.dni, COUNT(historias.dni) AS 'cantidad'
																										FROM historias
																										GROUP BY historias.dni
																								) AS temp2
								) AND (p.dni = temp.dni)
									AND (p.dni = a.dni)
									AND (a.sigla = o.sigla)
									AND (a.sigla = pp.sigla)
									AND (a.nroplan = pp.nroplan)
									AND (a.sigla = c.sigla)
									AND (a.nroplan = c.nroplan)
			)
