/*4*/

/*
1.	Crear un procedimiento para que proyecte los estudios realizados en un determinado mes.
input: mes y año.
¬	Proyectar los datos del afiliado y los de los estudios realizados.

*/

create procedure estudios_mes
    @mes int,
    @ano int
    AS
      select h.dni, p.nombre, p.apellido, h.idestudio, e.estudio, h.fecha
        from historias h inner join pacientes p on h.dni = p.dni
          inner join estudios e on e.idestudio = h.idestudio
        where MONTH(h.fecha) = @mes and YEAR(h.fecha) = @ano
GO
