/*16*/
  create view vw_nomina_de_medicos as (
    select
        case
          when sexo='masculino' then 'dr.'+ ' '+lower(nombre)+' '+upper(apellido)
          else 'dra.'+ ' '+lower(nombre)+' '+upper(apellido)
        end
        as NombreMedico
        from medicos
        )
