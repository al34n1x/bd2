/*10*/
create view vw_historias_de_estudios as (
    select p.nombre as Nombre, p.apellido as Apellido, e.estudio as Estudio, i.instituto as Instituto, m.matricula as Matricula, m.nombre as NombreMedico, h.fecha as FechaEstudio, h.sigla as Sigla, h.observaciones as Observaciones
        from pacientes as p join historias as h on p.dni = h.dni
        join estudios as e on h.idestudio = e.idestudio
        join institutos as i on h.idinstituto = i.idinstituto
        join medicos as m on h.matricula = m.matricula
        group by p.nombre, p.apellido, e.estudio, i.instituto, m.matricula, m.nombre, h.fecha, h.sigla, h.observaciones)
