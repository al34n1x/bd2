/*17*/

/*

Crear un Trigger que controle que un mismo estudio tenga
hasta un máximo de 6 institutos donde pueda realizarse.

*/

CREATE TRIGGER control_Estudio_Instituto ON precios FOR INSERT AS
	declare @estudio int
	declare @cantidad int

	SELECT @estudio = idestudio
		FROM estudios
		WHERE idestudio IN (SELECT idestudio FROM inserted)

	SELECT @cantidad = COUNT(idestudio)
		FROM precios
		WHERE idestudio = @estudio
		GROUP BY idestudio

	if (@cantidad <=6)
		begin
			print 'SE ha insertado el estudio solicitado'
		end
	else
		begin
			DELETE FROM precios WHERE EXISTS( SELECT * FROM inserted)
		end
