/*3*/
/*Cuáles son los pacientes que se realizaron más estudios.*/

SELECT p.dni as DNI, p.nombre as Nombre, p.apellido as Apellido, p.sexo as Sexo, p.nacimiento as FechaNac
FROM pacientes p
WHERE EXISTS (
							SELECT *
								FROM
										( SELECT historias.dni, COUNT(historias.dni) As cantidad
												FROM historias
												WHERE (p.dni = historias.dni)
												GROUP BY historias.dni
										)AS temp
							 WHERE temp.cantidad = (
																				SELECT MAX(temp2.cantidad)
																					FROM
																							( SELECT historias.dni, COUNT(historias.dni) As cantidad
																									FROM historias
																									GROUP BY historias.dni
																							) As temp2
																		)
						)
