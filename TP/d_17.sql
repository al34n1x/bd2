/*17*/
create view vw_estudios_en_tres_meses as(
    select e.estudio as Estudio,h.fecha as Fecha
        from estudios as e join historias as h on e.idestudio=h.idestudio
        where datediff(day,h.fecha,getdate()) between 0 and 90
                )
