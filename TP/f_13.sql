/*13*/

/*

Crear un procedimiento que devuelva el precio mínimo y el precio máximo que debe abonar a una obra social.
input: sigla de la obra social o prepaga
output: mínimo, máximo.
¬	Devolver en dos variables separadas el monto mínimo y máximo a ser cobrados por la obra social o prepaga.


*/
CREATE PROCEDURE PagoOOSS(
	@OOSS varchar(50),
	@minimo decimal output,
	@maximo decimal output)
	AS
		begin
		declare @table TABLE(minimo decimal, maximo decimal)
		INSERT INTO @table(minimo, maximo)
		(
			SELECT MIN(p.precio * c.cobertura / 100) AS 'Minimo', MAX(p.precio * c.cobertura / 100) AS 'Maximo'
				FROM historias h LEFT JOIN precios p ON
					(h.idinstituto = p.idinstituto) AND
					(h.idestudio = p.idestudio)
				LEFT JOIN afiliados a ON
					(h.dni = a.dni)
				LEFT JOIN coberturas c ON
					(h.sigla = c.sigla) AND
					(a.nroplan = c.nroplan)
				WHERE (UPPER(h.sigla) = UPPER(@OOSS)) AND (h.pagado = 'si')
	)
	SELECT @minimo = minimo
		FROM @table
	SELECT @maximo = maximo
		FROM @table
end
