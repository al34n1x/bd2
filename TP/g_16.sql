/*16*/
/*
Crear un Trigger que muestre el valor anterior y el nuevo valor de
cada columna que se actualizó en la tabla paciente.
*/

CREATE TRIGGER actualizarPaciente ON pacientes FOR INSERT AS
	declare @nombre_ant varchar(100)
	declare @nombre_nuevo varchar(100)
	declare @apellido_ant varchar(100)
	declare @apellido_nuevo varchar(100)
	declare @nacimiento_ant date
	declare @nacimiento_nuevo date
	declare @sexo_ant varchar(1)
	declare @sexo_nuevo varchar(1)

	SELECT
		@nombre_ant = nombre,
		@apellido_ant = apellido,
		@sexo_ant = sexo,
		@nacimiento_ant = nacimiento
	FROM deleted

	SELECT
		@nombre_nuevo = nombre,
		@apellido_nuevo = apellido,
		@sexo_nuevo = sexo,
		@nacimiento_nuevo = nacimiento
	FROM inserted

	print 'valores anteriores: ' + @nombre_ant + ' ' + @apellido_ant + ' ' + @sexo_ant + ' ' + CAST(@nacimiento_ant AS varchar)
	print 'valores nuevos: ' + @nombre_nuevo + ' ' + @apellido_nuevo + ' ' + @sexo_nuevo + ' ' + CAST(@nacimiento_nuevo AS varchar)
