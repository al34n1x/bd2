/*1*/
/*
Crear un procedimiento para ingresar el precio de un estudio.
input: nombre del estudio, nombre del instituto y precio.
¬	Si ya existe el registro en Precios debe actualizarlo.
¬	Si no existe debe crearlo.
¬	Si no existen el estudio o el instituto debe crearlos.

*/

create procedure Ingresar_Precio
    @instituto varchar(50),
    @estudio varchar(50),
    @precio decimal
        AS
          declare @idinstituto int
          declare @idestudio int
            if not exists(select * from institutos where instituto = @instituto)
                begin
                      insert into institutos values (@instituto,'si')
                end
            if not exists (select * from estudios where estudio = @estudio)
                begin
                      insert into estudios values (@estudio,'si')
                end

            set @idestudio = (select idestudio from estudios where estudio = @estudio)
            set @idinstituto = (select idinstituto from institutos where instituto = @instituto)

            if not exists (select * from precios where idestudio = @idestudio and idinstituto = @idinstituto)
                begin
                      insert into precios values (@idestudio,@idinstituto,@precio)
                end
            else
                      update precios set precio = @precio where idestudio = @idestudio and idinstituto = @idinstituto
GO
