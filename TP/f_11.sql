/*11*/

/*

Crear un procedimiento que devuelva el precio total a facturar y la cantidad de estudios intervinientes a una determinada obra social.
input: nombre de la obra social, periodo a liquidar.
output: precio neto, cantidad de estudios.
¬	Devuelve en dos variables el neto a facturar a la obra social o prepaga y la cantidad de estudios que abarca para un determinado período.


*/

CREATE PROCEDURE FacturarOOSS(
	@OOSS varchar(50),
	@inicio Date,
	@fin Date,
	@importe money output,
	@cantidad int output)
	AS
	begin
		declare @table TABLE (cantidad int, total int)
		declare @sigla varchar(50)
		set @sigla = (select sigla from OOSS where nombre = @ooss)
		INSERT INTO @table (cantidad, total)
			(
					SELECT COUNT(h.dni), SUM(IsNull(p.precio, 0) - IsNull(c.cobertura, 0)) AS 'Total'
						FROM historias h LEFT JOIN precios p ON
							(h.idinstituto = p.idinstituto) AND
							(h.idestudio = p.idestudio)
						LEFT JOIN afiliados a ON
							(h.dni = a.dni)
						LEFT JOIN coberturas c ON
							(h.sigla = c.sigla) AND
							(a.nroplan = c.nroplan) AND
							(h.idestudio = c.idestudio)
					WHERE
							(h.pagado = 'si') AND
							(h.sigla = @sigla) AND
							(h.fecha > @inicio) AND
							(h.fecha < @fin)

	)
	SELECT @importe = total
		FROM @table

	SELECT @cantidad = cantidad
		FROM @table
end
