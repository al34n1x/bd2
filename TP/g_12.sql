/*12*/

/*
Crear un Trigger que modifique la condición de un medico activo sin eliminarlo.
El trigger no debe permitir eliminar a un médico, se lo debe registrar como inactivo.
*/

CREATE TRIGGER eliminar_medico ON medicos INSTEAD OF DELETE AS
	declare @matricula int
	SELECT @matricula = matricula
		FROM deleted
		UPDATE medicos
		SET activo = 'si'
		WHERE (matricula = @matricula)
