/*8*/

create view vw_cantidad_estudios_por_instituto as (
    select  e.estudio as Estudio, i.instituto as Instituto, count (h.idestudio) as Cantidad
        from estudios as e join historias as h on e.idestudio = h.idestudio
            join institutos as i on h.idinstituto = i.idinstituto
            group by h.idinstituto, e.estudio, i.instituto
        )
