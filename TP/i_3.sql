/*3*/

/*
Definir un Cursor que liste la cantidad estudios realizados mostrando parciales por paciente y por instituto, conforme al siguiente detalle:
Datos del paciente
Nombre del Instituto 		Cantidad de estudios.
Total de estudios (realizados por el paciente)
*/

declare @paciente_dni int
declare @paciente_nombre varchar(50)
declare @paciente_apellido varchar(50)
declare @paciente_sexo varchar(1)
declare @paciente_nacimiento date
declare @instituto varchar(50)
declare @cantidad int
declare @total int
declare @total_acumulado int
set @total_acumulado = 0
declare cursor_pacientes CURSOR FOR (
										SELECT
											p.dni,
											p.apellido,
											p.nombre,
											p.sexo,
											p.nacimiento
										FROM
											pacientes p
									)

open cursor_pacientes
fetch next from cursor_pacientes INTO @paciente_dni, @paciente_apellido, @paciente_nombre, @paciente_sexo, @paciente_nacimiento
while (@@FETCH_STATUS = 0)
begin
	print '////////////////////////////////////////////////////////'
	print 'Datos del Paciente'
	print 'Apellido: ' + @paciente_apellido
	print 'Nombre: ' + @paciente_nombre
	print 'Sexo: ' + @paciente_sexo
	print 'Fecha de Nacimiento: ' + CAST(@paciente_nacimiento AS varchar(20))
	print 'Nombre del Instituto' + CHAR(9) + 'Cantidad de estudio'
	declare cursor_historias CURSOR FOR (
											SELECT
												i.instituto,
												COUNT(h.idinstituto)
												FROM historias h LEFT JOIN institutos i ON (h.idinstituto = i.idinstituto)
												WHERE h.dni = @paciente_dni
												GROUP BY i.instituto, h.idinstituto
										)
	open cursor_historias
	fetch next from cursor_historias INTO @instituto, @cantidad
	set @total = 0
	while (@@FETCH_STATUS = 0)
	begin
		print @instituto + CHAR(9) + CAST(@cantidad AS varchar(50))
		set @total = @total + @cantidad
		fetch next from cursor_historias INTO @instituto, @cantidad
	end
	close cursor_historias
	deallocate cursor_historias
	print 'Total de estudio: ' + CAST(@total AS varchar(50))
	set @total_acumulado = @total_acumulado + @total
	fetch next from cursor_pacientes INTO @paciente_dni, @paciente_apellido, @paciente_nombre, @paciente_sexo, @paciente_nacimiento
end
print 'Total de estudios realizados: ' + CAST(@total_acumulado AS varchar(50))
close cursor_pacientes
deallocate cursor_pacientes
