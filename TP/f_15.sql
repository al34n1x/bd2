/*15*/
/*
Crear un procedimiento que devuelva la cantidad de pacientes y médicos que efectuaron estudios en un determinado período.
input: / output: dos enteros.
¬	Ingresar período a consultar (mes y año)
¬	Retornar cantidad de pacientes que se realizaron uno o más estudios y cantidad de médicos solicitantes de los mismos, en dos variables.


*/
create procedure estudiosPacientesMedicos
    @ano int,
    @mes int,
    @pacientes int output,
    @medicos int output
    AS
      set @pacientes = (select count(dni) from historias where year(fecha) = @ano and MONTH(fecha) = @mes)
      print @pacientes
      set @medicos = (select count(matricula) from historias where year(fecha) = @ano and MONTH(fecha) = @mes)
      print @medicos
GO
