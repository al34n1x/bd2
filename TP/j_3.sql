/*3*/

/*

Definir una transacción que elimine lógicamente de la Base de Datos a todos los médicos de una determinada especialidad. Se anidarán las stored procedures que se necesiten para completar la transacción, que debe tener en cuenta lo siguiente:
¬	La eliminación del médico debe ser lógica conforme al trigger asociado a la acción de delete. (TP5)
¬	No se realizará la eliminación del médico si el mismo posee otra especialidad.
¬	Las historias no serán eliminadas.
¬	Crear una tabla temporaria donde se registrarán las referencias a los médicos e historias que intervinieron en el proceso.
¬	Emitir un listado de los datos involucrados en el proceso (grabados en la tabla temporaria), según el siguiente formato:

Usuario responsable XXXXXXXXX
ELIMINACION DE MEDICOS DE LA ESPECIALIDAD XXXXXXXXXXX
---------------------------------------------------------------------------
Dr(a) Nombre Apellido 	estudios que indicó
Total 				de estudios indicados
Total de estudios especialidad

¬	En caso de error se anulará la transacción presentando el mensaje correspondiente.(Un error en la emisión del listado no debe anular la transacción de eliminación).


*/


CREATE PROCEDURE delete_medico_especialidad (@especialidad varchar(50)) AS
begin
	declare @idespecialidad int
	declare @matricula varchar(50)
	declare @nombre varchar(50)
	declare @apellido varchar(50)
	declare @estudio varchar(50)
	declare @cantidad int
	SELECT
		@idespecialidad = e.idespecialidad
	FROM
		especialidades e
	WHERE
		e.especialidad = @especialidad
	declare cursor_medicos CURSOR FOR (
								SELECT
									temp1.matricula
								FROM
									espemedi AS temp1
								WHERE
									EXISTS(
											SELECT
												*
											FROM
												espemedi AS temp2
											WHERE
												(temp2.idespecialidad = 20) AND
												(temp1.matricula = temp2.matricula)
											)
								GROUP BY
									temp1.matricula
								HAVING
									COUNT(temp1.matricula) > 1
								)
	open cursor_medicos
	fetch next from cursor_medicos INTO @matricula
	while (@@FETCH_STATUS = 0)
	begin
		DELETE FROM medicos m
		WHERE
			(m.matricula = @matricula)
		if NOT EXISTS (
						SELECT *
							FROM sysobjects
							WHERE ([name] = 'ex_pacientes') AND ([type] = 'U')
						)
		begin
			CREATE TABLE temp_estudios
			(
				nombre varchar(50),
				apellido varchar(50),
				estudio varchar(50),
				cantidad int
			)
		end
		INSERT INTO temp_estudios (nombre, apellido, estudio, cantidad)
		(
			SELECT
				m.nombre,
				m.apellido,
				estudios.estudio,
				COUNT(estudios.estudio)
			FROM medicos m  JOIN espemedi e ON (m.matricula = e.matricula)
				JOIN especialidades ON (e.idespecialidad = especialidades.idespecialidad)
				JOIN estuespe ON (especialidades.idespecialidad = estuespe.idespecialidad)
				JOIN estudios ON (estuespe.idestudio = estudios.idestudio)
			WHERE m.matricula = @matricula
			GROUP BY m.nombre, m.apellido, e.estudio
		)
		declare cursor_estudios CURSOR FOR
									(
										SELECT
											nombre,
											apellido,
											estudio,
											cantidad
										FROM temp_estudios
									)
		open cursor_estudios
		fetch cursor_estudios INTO @nombre, @apellido, @estudio, @cantidad
		while (@@FETCH_STATUS = 0)
		begin
			print 'Usuario responsable ' + CURRENT_USER
			print 'ELIMINACION MEDICOS' + @especialidad
			print '////////////////////////////////////////////////////////'
			print 'Dr/a' + @nombre + ' ' + @apellido + ' ' + @estudio
			print 'Total ' + CAST(@cantidad AS varchar(50))
			fetch cursor_estudios INTO @nombre, @apellido, @estudio, @cantidad
		end
		SELECT
			@cantidad = COUNT(*)
		FROM
			temp_estudios
		print 'Total estudios ' + CAST(@cantidad AS varchar(50))
		DROP TABLE temp_estudios
		fetch next from cursor_medicos INTO @matricula
		close cursor_estudios
		deallocate cursor_estudios
	end
	close cursor_medicos
	deallocate cursor_medicos
end
