/*1. Crear un procedimiento PA999999 que reciba el nombre de dos tablas y realice las siguientes acciones:
Verificar que la primera tabla ingresada existe y la segunda tabla no.
Si alguna de las condiciones antes mencionadas no se cumple, terminar el procedimiento.
Crear una tabla con el nombre ingresado que no existe en el catálogo.
La misma deberá tener los siguientes campos: campo cadena de 40, tipoDato cadena 40, posiciones entero, decimales entero, orden entero
Hacer una consulta al catálogo para obtener el nombre de las columnas de la tabla ingresada (la existente en el catálogo),
junto con el nombre del tipo de dato, la cantidad de posiciones totales, la cantidad de posiciones decimales para cada uno de los campos
y el orden de los mismos insertando los resultados de la consulta en la tabla creada.
*/

create procedure PA999999
  @table1 varchar (50)
  @tabla2 varchar (50)

  AS
  begin
    declare @sentencia nvarchar(1000)
    if not exist(select * from sysobjects where = 'U' and name = @table1)
      return 1;
    if exist(select * from sysobjects where = 'U' and name = @table2)
      return 1;
    else
      set @sentencia = 'create table' +@table2 + '(' + 'Campo varchar(40), tipoDato varchar (40), Posiciones int,'
      set @sentencia = @sentencia + 'Decimales int, orden int)'
      exec sp.executesql @sentencia
  GO
