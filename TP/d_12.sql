/*12*/
create view vw_ooss_pacientes as (
    select o.nombre as NombreOOSS, ppp.nombre as NombrePlanes, pp.activo as EstadoPlan, p.nombre as Nombre, p.apellido as Apellido, a.dni as DNI
        from ooss as o join planes as pp on o.sigla = pp.sigla
        join planes as ppp on o.sigla = ppp.sigla
        join afiliados as a on o.sigla = a.sigla
        join pacientes as p on a.dni = p.dni
        group by o.nombre, ppp.nombre, pp.activo, p.nombre, p.apellido, a.dni
        )
