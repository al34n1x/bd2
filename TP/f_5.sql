/*5*/
/*1.	Crear un procedimiento que proyecte los pacientes según un rango de edad.
input: edad mínima y edad máxima.
¬	Proyectar los datos del paciente.
*/

create procedure rango_edad
    @edadmin int,
    @edadmax int
      AS
        select *
        from pacientes
        where YEAR(nacimiento) between DATEPART(yyyy,GETDATE()) - @edadmax and DATEPART(yyyy,getdate()) - @edadmin

GO
