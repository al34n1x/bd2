/*1*/

/*
Definir un Cursor que liste la ficha de los pacientes de los últimos seis meses conforme al siguiente formato de salida:
Datos del paciente.
Identificación del médico.
Detalle de los estudios realizados.
*/

declare @dni int
declare @idestudio int
declare @fecha_estudio date
declare @matricula int
declare @paciente_nombre varchar(50)
declare @paciente_apellido varchar(50)
declare @paciente_sexo varchar(1)
declare @paciente_nacimiento date
declare @medico_nombre varchar(50)
declare @medico_apellido varchar(50)
declare @especialidad varchar(100)
declare @estudio varchar(100)
declare @matricula_anterior int
declare @paciente_anterior int
set @matricula_anterior = null
set @paciente_anterior = null
declare cursor_historias CURSOR FOR
									SELECT
										h.dni,
										h.idestudio,
										h.fecha,
										h.matricula
									FROM historias h
									WHERE DATEDIFF(m, GETDATE(), fecha) >= 6
									ORDER BY h.dni, h.matricula;
open cursor_historias
fetch next from cursor_historias INTO @dni, @idestudio, @fecha_estudio, @matricula
while (@@FETCH_STATUS = 0)
begin
	if ((@paciente_anterior IS NULL) or (@paciente_anterior <> @dni))
	begin
		SELECT
			@paciente_nombre = p.nombre,
			@paciente_apellido = p.apellido,
			@paciente_sexo = p.sexo,
			@paciente_nacimiento = p.nacimiento
		FROM
			pacientes p
		WHERE
			p.dni = @dni
		print '////////////////////////////////////////////////////////'
		print 'Datos del Paciente'
		print 'Apellido: ' + @paciente_apellido
		print 'Nombre: ' + @paciente_nombre
		print 'Sexo: ' + @paciente_sexo
		print 'Fecha Nacimiento: ' + CAST(@paciente_nacimiento AS varchar(20))
		set @paciente_anterior = @dni
		set @matricula_anterior = null
	end
	if ((@matricula_anterior IS NULL) or (@matricula_anterior <> @matricula))
	begin
		SELECT
			@medico_nombre = m.nombre,
			@medico_apellido = medimcos.apellido,
			@especialidad = ee.especialidad
		FROM medicos m LEFT JOIN espemedi e ON (m.matricula = e.matricula)
		LEFT JOIN especialidades ee ON (e.idespecialidad = ee.idespecialidad)
		WHERE m.matricula = @matricula
		print '////////////////////////////////////////////////////////'
		print 'Datos correspondientes al profesional:'
		print 'Apellido: ' + @medico_apellido
		print 'Nombre: ' + @medico_nombre
		print 'Matricula: ' + CAST(@matricula AS varchar(50))
		print 'Especialidad: ' + @especialidad
		print '////////////////////////////////////////////////////////'
		print 'Estudios'
		set @matricula_anterior = @matricula
	end
	SELECT
		@estudio = estudios.estudio
		FROM estudios
		WHERE estudios.idestudio = @idestudio
	print @estudio
	fetch next from cursor_historias INTO @dni, @idestudio, @fecha_estudio, @matricula
end
close cursor_historias
deallocate cursor_historias
