/*12*/

/*Crear un procedimiento que devuelva el monto a abonar de un paciente moroso.
input: dni del paciente, estudio realizado, fecha de realización, punitorio (mensual).
output: precio neto a pagar.
¬	Obtener punitorio diario y precio a abonar.
¬	Devuelve precio + punitorio en una variable.
*/
CREATE PROCEDURE EstudiosMora(
	@dni int,
	@estudio varchar(50),
	@fecha Date,
	@punitorio decimal,
	@precio decimal output,
	@interes decimal output)
	AS
		begin
		declare @idestudio int
		declare @table TABLE (precio decimal, interes decimal)
		set @idestudio = (select idestudio from estudios where estudio = @estudio)
		INSERT INTO @table (precio, interes)
			(
				SELECT precios.precio, (DATEDIFF(dd, historias.fecha, GETDATE()) * @punitorio / 30) AS 'interes'
					FROM historias LEFT JOIN precios ON
						(historias.idinstituto = precios.idinstituto) AND
						(historias.idestudio = precios.idestudio)
					WHERE (historias.pagado = 0) AND
								(historias.dni = @dni) AND
								(historias.idestudio = dbo.getEstudio(@estudio)) AND
								(historias.fecha = @fecha)
		)

	SELECT @precio = precio
		FROM @table

	SELECT @interes = interes
		FROM	@table
end
