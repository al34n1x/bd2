/*9*/

/*Definir una función que devuelva las obras sociales que cubren un determinado
estudio en todos los planes que tiene y que se realizan en algún instituto
registrado en la base.
input: nombre del estudio.
output: una Tabla que proyecta la obra social y la categoría.
*/

CREATE FUNCTION ooss_cobertura_estudio (@estudio varchar(50)) 
RETURNS @table table (
	ooss varchar(50),
	categoria varchar(50))
AS
	begin
		INSERT INTO @table (ooss, categoria)
		(
			SELECT nombre, categoria
			FROM ooss o LEFT JOIN (SELECT c.idestudio, c.sigla
														FROM precios p LEFT JOIN estudios e ON (p.idestudio = e.idestudio)
														LEFT JOIN coberturas c ON (p.idestudio = c.idestudio)
														WHERE e.estudio = @estudio
														GROUP BY c.idestudio, c.sigla
														HAVING COUNT(c.sigla) = (
																																SELECT COUNT(sigla) as est_repetido
																																	FROM planes
																																	WHERE c.sigla = sigla
																																	GROUP BY sigla
																  														)
												) as temp ON (o.sigla = temp.sigla)
		)
	return
end
