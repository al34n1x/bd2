/*1*/
/*Cuál es el menor precio de un estudio en cada instituto.
Indicar el nombre del estudio y el nombre del instituto, ordenando el resultado por estudio e instituto.
*/
SELECT i.instituto, e.estudio
	FROM institutos i, estudios e
	WHERE EXISTS(
			SELECT precios.idinstituto, precios.idestudio, precios.precio
			FROM(
						SELECT precios.idinstituto, MIN(precios.precio) AS 'p_min'
							FROM precios
							GROUP BY precios.idinstituto
					) AS temp LEFT JOIN precios ON (precios.idinstituto = temp.idinstituto) AND (precios.precio = temp.p_min)
			WHERE (i.idinstituto = precios.idinstituto) AND (e.idestudio = precios.idestudio)
		 )
