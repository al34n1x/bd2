/*2*/
/*

1.	Definir una transacción que elimine de la Base de Datos a un paciente. Se anidarán las stored procedures que se necesiten para completar la transacción, que debe incluir los siguientes procesos:
¬	Eliminar trigger asociado a la tabla historias para la acción de delete, previa verificación de existencia del mismo.
¬	Volver a afectar dicho trigger al finalizar el proceso de eliminación.
¬	Crear las tablas ex_pacientes y ex_historias (si no existen) y grabar los datos intervinientes en la eliminación. (los datos correspondientes a la afiliación del paciente se eliminan pero no se registran). Incluír en la tabla ex_pacientes la registración del usuario que invocó la transacción y la fecha.
¬	Se deben eliminar en cadena todas las tablas afectadas al proceso, en caso de error se anulará la transacción presentando el mensaje correspondiente y devolviendo un código de error.


*/

CREATE PROCEDURE borrar_paciente (@dni int) AS
begin
	declare @error int
	declare @declaracion nvarchar(500)
	begin transaction
	set @error = 0
	if EXISTS (
				SELECT *
					FROM sysobjects
					WHERE ([name] = 'delete_historias') AND ([type] = 'TR')
					)
	begin
		DROP TRIGGER delete_historias
	end
		if NOT EXISTS ( SELECT *
											FROM sysobjects
											WHERE ([name] = 'antiguos_pacientes') AND ([type] = 'U')
										)
		begin
			CREATE TABLE antiguos_pacientes
			(
				dni int,
				nombre varchar(50),
				apellido varchar(50),
				sexo varchar,
				nacimiento date,
				usuario varchar(50),
				fecha date,
				CONSTRAINT pk_expacientes PRIMARY KEY (dni),
				CONSTRAINT chk_expacientes CHECK (sexo IN ('f', 'm'))
			)
		end
		if NOT EXISTS (
						SELECT *
							FROM sysobjects
							WHERE ([name] = 'antiguas_historias') AND ([type] = 'U')
						)
		begin
			CREATE TABLE antiguas_historias
			(
				dni int,
				idestudio int,
				idinstituto int,
				fecha date,
				matricula int,
				sigla varchar(50),
				pagado binary,
				observaciones varchar(500)
				CONSTRAINT pk_exhistorias PRIMARY KEY (dni, idestudio, fecha),
				CONSTRAINT fk_exhistoriasmedicos FOREIGN KEY (matricula) REFERENCES medicos(matricula),
				CONSTRAINT fk_exhistoriasprecios FOREIGN KEY (idestudio, idinstituto) REFERENCES precios(idestudio, idinstituto),
				CONSTRAINT fk_exhistoriaspacientes FOREIGN KEY (dni) REFERENCES antiguos_pacientes(dni),
			)
		end
		INSERT INTO ex_pacientes (dni, apellido, nombre, sexo, nacimiento, usuario, fecha)
		(
			SELECT
				pacientes.dni,
				pacientes.apellido,
				pacientes.nombre,
				pacientes.sexo,
				pacientes.nacimiento,
				CURRENT_USER,
				GETDATE()
			FROM
				pacientes
			WHERE
				(pacientes.dni = @dni)
		)
		INSERT INTO ex_historias (dni, idestudio, idinstituto, fecha, matricula, sigla, pagado, observaciones)
		(
			SELECT
				historias.dni,
				historias.idestudio,
				historias.idinstituto,
				historias.fecha,
				historias.matricula,
				historias.sigla,
				historias.pagado,
				historias.observaciones
			FROM
				historias
			WHERE
				(historias.dni = @dni)
		)
		DELETE FROM historias
		WHERE
			(historias.dni = @dni)
		if (@@ERROR <> 0)
		begin
			set @error = 1
			print 'Error al eliminar los registros de la tabla historias del paciente con dni ' + CAST(@dni AS varchar(50))
		end
		DELETE FROM afiliados
		WHERE
			(afiliados.dni = @dni)
		if (@@ERROR <> 0)
		begin
			set @error = 1
			print 'Error al eliminar el paciente con dni ' + CAST(@dni AS varchar(50)) + ' de la tabla de afiliados'
		end
		DELETE FROM pacientes
		WHERE
			(pacientes.dni = @dni)
		if (@@ERROR <> 0)
		begin
			set @error = 1
			print 'Error al eliminar el paciente con dni ' + CAST(@dni AS varchar(50)) + ' de la tabla de pacientes'
		end
		if (@error = 0)
		begin
			COMMIT
			set @statement = 'CREATE TRIGGER delete_historias ON historias INSTEAD OF DELETE AS	print ' + char(39) + 'No se permite la eliminaci�n de historias de pacientes' + char(39)
			exec sp_executesql @statement
		end
		else
		begin
			ROLLBACK
		end
end
