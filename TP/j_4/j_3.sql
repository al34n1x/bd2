/*4_3*/
CREATE PROCEDURE ingresarAfiliado(@dni int, @sigla varchar(50), @plan int, @afiliado int) AS
begin
	begin transaction
	exec updateAfiliado @dni, @sigla, @plan, @afiliado
	if (@@ERROR = 0)
	begin
		commit
	end
	else
	begin
		rollback
		begin transaction
		exec insertAfiliado @dni, @sigla, @plan, @afiliado
		if (@@ERROR = 0)
		begin
			commit
		end
		else
		begin
			rollback
			print 'combinaci�n de obra social / prepaga y n�mero de plan no v�lidos'
		end
	end
end
