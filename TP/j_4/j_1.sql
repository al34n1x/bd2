/*4_1*/
CREATE PROCEDURE ingresarPrecio(@estudio varchar(100), @instituto varchar(100), @precio int) AS
begin
	begin transaction
	exec updatePrecio @estudio, @instituto, @precio
	if (@@ERROR = 0)
	begin
		commit
	end
	else
	begin
		rollback
		begin transaction
		exec insertInstituto @instituto
		if (@@ERROR = 0)
		begin
			commit
		end
		else
		begin
			rollback
			begin transaction
			exec insertEstudio @estudio
			if (@@ERROR = 0)
			begin
				commit
			end
			else
			begin
				rollback
			end
		end
	end
	begin transaction
	exec insertPrecio @estudio, @instituto, @precio
	if (@@ERROR = 0)
	begin
		commit
	end
	else
	begin
		rollback
	end
end
