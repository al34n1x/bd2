/*4_2*/
CREATE PROCEDURE ingresarEstudiosProgramados(@estudio varchar(100), @dni int, @matricula int, @instituto varchar(50), @sigla varchar(50), @cantidad int, @dias int) AS
begin
	declare @proceder binary
	declare @counter int
	declare @fecha date
	set @counter = 0
	set @error = 0
	set @fecha = GETDATE()
	begin transaction
	while (@counter < @cantidad)
	begin
		set @fecha = DATEADD(d, @dias, @fecha)
		exec insertHistoria @estudio, @dni, @matricula, @instituto, @sigla, @fecha
		set @error = @error + @@ERROR
		set @counter = @counter + 1
	end
	if (@error = 0)
	begin
		commit
	end
	else
	begin
		rollback
		if (dbo.isEstudio(@estudio) = 0)
		begin
			print @estudio + ' no es un estudio v�lido'
		end
		if (dbo.isPaciente(@dni) = 0)
		begin
			print @dni + ' no pertenece a un paciente registrado'
		end
		if (dbo.isMedico(@matricula) = 0)
		begin
			print @matricula + ' no pertenece a un m�dico registrado'
		end
		if (dbo.isInstituto(@instituto) = 0)
		begin
			print @instituto + ' no es un instituto registrado'
		end
		if (dbo.isOOSS(@sigla) = 0)
		begin
			print @sigla + ' no es una obra social / prepaga soportada'
		end
	end
end
