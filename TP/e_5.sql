/*5*/

/*Cuál es la especialidad que más estudios tiene recetados.
Indicar el nombre de la especialidad y la cantidad de estudios
*/
SELECT e.especialidad
	FROM especialidades e
	WHERE EXISTS(
								SELECT temp.idespecialidad, temp.cantidad
									FROM (
												 SELECT ee.idespecialidad, COUNT(ee.idespecialidad) AS 'cantidad'
												 	FROM estuespe ee
													GROUP BY ee.idespecialidad
											) AS temp
									WHERE temp.cantidad = (
																					SELECT MAX(temp2.cantidad)
																						FROM (
																									SELECT estuespe.idespecialidad, COUNT(estuespe.idespecialidad) AS 'cantidad'
																										FROM estuespe
																										GROUP BY estuespe.idespecialidad
																				) AS temp2
								) AND (e.idespecialidad = temp.idespecialidad)
			)
