/*9*/
/*Cuál es el gasto de dinero realizado por cada obra social sumando los estudios de todos sus afiliados.
*/
SELECT o.nombre, SUM(temp.precio * c.cobertura / 100) AS 'Importe'
	FROM historias h LEFT JOIN afiliados a ON (h.dni = a.dni) AND (h.sigla = a.sigla)
	LEFT JOIN coberturas c ON
		(c.sigla = h.sigla) AND
		(c.nroplan = a.nroplan) AND
		(c.idestudio = h.idestudio)
	LEFT JOIN ooss o ON (h.sigla = o.sigla)
	LEFT JOIN (
							SELECT *
								FROM precios
								WHERE EXISTS(
															SELECT *
																FROM historias
																WHERE (historias.pagado = 'si') AND (precios.idestudio = historias.idestudio)
																	AND (precios.idinstituto = historias.idinstituto)
														)
						) AS temp ON (h.idestudio = temp.idestudio) AND (h.idinstituto = temp.idinstituto)
GROUP BY o.nombre
