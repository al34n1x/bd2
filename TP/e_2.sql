/*2*/
/*Cuáles son los pacientes que no poseen cobertura.*/

SELECT p.dni as DNI, p.nombre as Nombre, p.apellido as Apellido, p.sexo as Sexo, p.nacimiento as FechaNac
	FROM pacientes p LEFT JOIN afiliados a ON (p.dni = a.dni)
	LEFT JOIN planes pp ON (a.sigla = pp.sigla) AND (a.nroplan = pp.nroplan)
	WHERE	NOT EXISTS (
									SELECT *
										FROM coberturas
										WHERE (pp.sigla = coberturas.sigla) AND (pp.nroplan = coberturas.nroplan)
									)
