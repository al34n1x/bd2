/*3*/

/*
Definir las siguientes funciones que devuelva una lista ordenada alfabéticamente de:
• Obras sociales.
• Especialidades
• Institutos.
• Estudios.
OUTPUT: una Tabla conteniendo la información solicitada.
*/

CREATE FUNCTION ObrasSociales() RETURNS @table TABLE (nombre varchar(50)) AS
begin
	INSERT INTO @table (nombre)
	SELECT o.nombre
		FROM ooss o
	return
end

CREATE FUNCTION Especialidades() RETURNS @table TABLE (nombre varchar(50)) AS
begin
	INSERT INTO @table (nombre)
	SELECT e.especialidad
		FROM especialidades e
	return
end

CREATE FUNCTION Institutos() RETURNS @table TABLE (nombre varchar(50)) AS
begin
	INSERT INTO @table (nombre)
	SELECT i.instituto
		FROM institutos i
	return
end

CREATE FUNCTION Estudios() RETURNS @table TABLE (nombre varchar(50)) AS
begin
	INSERT INTO @table (nombre)
	SELECT e.estudio
		FROM estudios e
	return
end
