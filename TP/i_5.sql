/*5*/

/*

Crear una Stored Procedure que defina un Cursor que liste el resumen mensual de los importes a cargo de una obra social.
Input: nombre de la obra social, mes y año a liquidar.
Obra social
Nombre del Instituto
Detalle del estudio

Subtotal del Instituto

Total de la obra social


*/

CREATE PROCEDURE OOSS_mensual(@OOSS varchar(50), @mes int, @ano int) AS
begin
	declare @idestudio int
	declare @idinstituto int
	declare @fecha_estudio date
	declare @pagado binary
	declare @estudio varchar(50)
	declare @instituto varchar(50)
	declare @precio decimal(18,0)
	declare @subtotal int
	declare @total int
	declare @instituto_anterior int
	declare @aux_sigla varchar(50)
	set @instituto_anterior = null
	set @total = 0
	set @aux_sigla = (select sigla from ooss where nombre = @ooss)
	print '////////////////////////////////////////////////////////'
	print 'Obra Social: ' + @aux_sigla
	declare cursor_historias CURSOR FOR
										SELECT
											h.idestudio,
											h.idinstituto,
											h.fecha,
											h.pagado
										FROM
											historias h
										WHERE
											(h.sigla = dbo.getOOSS(@OOSS)) AND
											(MONTH(h.fecha) = @mes) AND
											(YEAR(h.fecha) = @ano)
										ORDER BY
											h.idinstituto;
	open cursor_historias
	fetch next from cursor_historias INTO @idestudio, @idinstituto, @fecha_estudio, @pagado
	while (@@FETCH_STATUS = 0)
	begin
		if ((@instituto_anterior IS NULL) or (@instituto_anterior <> @idinstituto))
		begin
			SELECT
				@instituto = i.instituto
			FROM
				institutos i
			WHERE
				i.idinstituto = @idinstituto
			print '////////////////////////////////////////////////////////'
			print 'Nombre del Instituto: ' + @instituto
			print 'Fecha' + CHAR(9) + 'Estudio' + CHAR(9) + 'Precio'
			set @instituto_anterior = @idinstituto
			set @subtotal = 0
		end
		SELECT
			@estudio = e.estudio
		FROM
			estudios e
		WHERE
			e.idestudio = @idestudio
		SELECT
			@precio = p.precio
		FROM
			precios p
		WHERE
			(p.idinstituto = @idinstituto) AND
			(p.idestudio = @idestudio)
		set @subtotal = @subtotal + @precio
		set @total = @total + @precio
		print CAST(@fecha_estudio AS varchar(20)) + CHAR(9) + @estudio + CHAR(9) + CAST(@precio AS varchar(50))
		fetch next from cursor_historias INTO @idestudio, @idinstituto, @fecha_estudio, @pagado
		if ((@@FETCH_STATUS <> 0) or (@instituto_anterior <> @idinstituto))
		begin
			print 'Subtotal Instituto: ' + CAST(@subtotal AS varchar(50))
		end
	end
	print '////////////////////////////////////////////////////////'
	print 'Total Obra Social: ' + CAST(@total AS varchar(50))
	close cursor_historias
	deallocate cursor_historias
end
