/*11*/

/*
Crear un Trigger que indique un mensaje que no se permite eliminar las historias de los pacientes.
*/

CREATE TRIGGER eliminar_historias ON historias INSTEAD OF DELETE AS
	print 'No se permite la eliminacion de historias de pacientes'
