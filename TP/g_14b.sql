/*14b*/
CREATE TRIGGER idest ON estudios FOR INSERT AS
	declare @id int
	SELECT
		@id = MAX(idestudio)
		FROM estudios
		GROUP BY idestudio
	print 'estudio n�mero ' + CAST(@id AS varchar) + ' ha sido agregada a la tabla estudios'
