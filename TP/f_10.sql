/*10*/

/*
Crear un procedimiento que devuelva el precio total a liquidar a un determinado instituto.
input: nombre del instituto, periodo a liquidar.
output: precio neto.
¬	Devuelve el neto a liquidar al instituto para ese período en una variable.

*/

CREATE PROCEDURE liquidacionInstituto(
	@instituto varchar(50),
	@inicio Date,
	@fin Date,
	@importe money output)
	AS
		declare @idinstituto int
		set @idinstituto = (select idinstituto from institutos where instituto = @instituto)
		select @importe = SUM(total)
				FROM (
							SELECT SUM(IsNull(p.precio, 0) - IsNull(c.cobertura, 0)) AS 'total'
								FROM historias h
								LEFT JOIN precios p ON (h.idinstituto = p.idinstituto) AND (h.idestudio = p.idestudio)
								LEFT JOIN afiliados a ON (h.dni = a.dni)
								LEFT JOIN coberturas c ON
									(h.sigla = c.sigla) AND
									(a.nroplan = c.nroplan) AND
									(h.idestudio = c.idestudio)
								WHERE
									(h.idinstituto = @idinstituto) AND
									(h.fecha > @inicio) AND
									(h.fecha < @fin) AND
									(h.pagado = 'si')
						)
GO
