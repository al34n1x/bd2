/*4*/

/*1.	Definir un Cursor que liste la cantidad estudios solicitados mostrando parciales por estudio y por médico, y detalle de los estudios solicitados conforme al siguiente formato:
Datos del médico
Nombre del estudio
Fecha 		Paciente

Cantidad del estudio

Cantidad de estudios del médico
*/

declare @medico_matricula int
declare @medico_nombre varchar(50)
declare @medico_apellido varchar(50)
declare @idestudio int
declare @fecha date
declare @paciente_dni int
declare @paciente varchar(100)
declare @estudio varchar(50)
declare @estudio_anterior int
declare @cantidad int
declare @total int
set @total = 0
declare cursor_medicos CURSOR FOR (
										SELECT
											m.matricula,
											m.apellido,
											m.nombre
										FROM
											medicos m
									)

open cursor_medicos
fetch next from cursor_medicos INTO @medico_matricula, @medico_apellido, @medico_nombre
while (@@FETCH_STATUS = 0)
begin
	print '--------------------------------------------------'
	print 'Datos del Medico'
	print 'Apellido: ' + @medico_apellido
	print 'Nombre: ' + @medico_nombre
	declare cursor_historias CURSOR FOR
										SELECT
											h.idestudio,
											h.fecha,
											h.dni
										FROM
											historias h
										WHERE
											h.matricula = @medico_matricula
										ORDER BY
											h.idestudio,
											h.dni;
	open cursor_historias
	fetch next from cursor_historias INTO @idestudio, @fecha, @paciente_dni
	set @estudio_anterior = null
	while (@@FETCH_STATUS = 0)
	begin
		if ((@estudio_anterior IS NULL) or (@estudio_anterior <> @idestudio))
		begin
			SELECT
				@estudio = e.estudio
			FROM
				estudios e
			WHERE
				e.idestudio = @idestudio
			print 'Estudio: ' + @estudio
			print 'Fecha' + CHAR(9) + 'Paciente'
			set @estudio_anterior = @idestudio
			set @cantidad = 0
		end
		SELECT
			@paciente = p.apellido + ' ' + p.nombre
		FROM
			pacientes p
		WHERE
			p.dni = @paciente_dni
		print CAST(@fecha AS varchar(20)) + CHAR(9) + @paciente
		set @cantidad = @cantidad + 1
		fetch next from cursor_historias INTO @idestudio, @fecha, @paciente_dni
		if (@estudio_anterior <> @idestudio)
		begin
			print 'Cantidad estudio: ' + CAST(@cantidad AS varchar(50))
			set @total = @total + @cantidad
		end
		if  (@@FETCH_STATUS <> 0)
		begin
			print 'Cantidad estudio: ' + CAST(@cantidad AS varchar(50))
			set @total = @total + @cantidad
			print 'Cantidad de estudios del profesional: ' + CAST(@total AS varchar(50))
			set @total = 0
		end
	end
	close cursor_historias
	deallocate cursor_historias
	fetch next from cursor_medicos INTO @medico_matricula, @medico_apellido, @medico_nombre
end
print 'Cantidad de estudios del profesional: ' + CAST(@total AS varchar(50))
close cursor_medicos
deallocate cursor_medicos
