/*7*/
create view vw_afiliados_con_una_cobertura as ( 
    select p.nombre,a.sigla,a.nroplan from pacientes as p inner join afiliados as a on p.dni=a.dni
    where p.dni in (
        select dni from afiliados group by dni
        having count(*)=1) )
