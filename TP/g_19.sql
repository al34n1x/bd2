/*19*/

/*Crear un Trigger que controle que un médico no indique un estudio a un
paciente que no sea afín con la especialidad del médico.
*/
CREATE TRIGGER control_Medico_Estudio ON historias FOR INSERT AS
	declare @estudio int
	declare @matricula int

	SELECT @estudio = idestudio
		FROM inserted

	SELECT @matricula = m.matricula
		FROM medicos m JOIN espemedi e ON (m.matricula = e.matricula)
		JOIN especialidades ee ON (e.idespecialidad = ee.idespecialidad)
		JOIN estuespe ON (ee.idespecialidad = estuespe.idespecialidad)
		WHERE (estuespe.idestudio = @estudio)

	if (@matricula IS NOT NULL)
	begin
		DELETE FROM historias
		WHERE EXISTS ( SELECT * FROM inserted )
	end

	else
		begin
			print 'Especialista Matricula Numero ' + @matricula + ' no puede solicitar el estudio'
		end
