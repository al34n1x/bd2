/*6*/

/*Cuál es el estudio que figura en más especialidades.*/

SELECT e.estudio
	FROM estudios e
	WHERE EXISTS(
								SELECT temp.idestudio
									FROM (
													SELECT ee.idestudio, COUNT(ee.idestudio) AS 'cantidad'
														FROM estuespe ee
														GROUP BY ee.idestudio
											 ) AS temp
								  WHERE temp.cantidad = (
																					SELECT MAX(temp2.cantidad)
																						FROM (
																										SELECT estuespe.idestudio, COUNT(estuespe.idestudio) AS 'cantidad'
																											FROM estuespe
																											GROUP BY estuespe.idestudio
																									) AS temp2
								) AND (e.idestudio = temp.idestudio)
							)
