/*5*/

/*

Definir una función que devuelva los estudios que no se realizaron en los últimos n días.
input: cantidad de días.
output: una Tabla de estudios.

*/

CREATE FUNCTION estudios_dias (@dias int) RETURNS @table TABLE (nombre varchar (50)) AS
begin
	INSERT INTO @table (nombre)
	(
		SELECT e.estudio
			FROM estudios e
		EXCEPT
		SELECT ee.estudio
			FROM estudios ee LEFT JOIN historias h ON (ee.idestudio = h.idestudio)
		WHERE (h.fecha >= DATEADD(dd, -@dias, GETDATE())) AND (h.fecha <= GETDATE())
	)
	return
end
