/*10*/
/*1.	Determine los 3 pacientes que pagaron en concepto de estudios
y que poseían cobertura a través de una obra social. Proyecte el nombre,
el apellido, la obra social, el plan y el importe abonado por el usuario.
*/

SELECT
	temp.nombre as Nombre,
	temp.apellido as Apellido,
	temp.OOSS as OOSS,
	temp.nroplan as NroPlan,
	temp.importe as Importe
FROM
	(
		SELECT DISTINCT
			ROW_NUMBER() OVER (ORDER BY p.apellido ASC) AS row_number,
			p.nombre,
			p.apellido,
			o.nombre AS 'OOSS',
			pp.nroplan,
			SUM(precios.precio) AS 'Importe'
		FROM pacientes p LEFT JOIN historias h
			ON (p.dni = h.dni)
			LEFT JOIN precios
			ON (h.idinstituto = precios.idinstituto) AND (h.idestudio = precios.idestudio),
			OOSS o, planes pp
		WHERE EXISTS(
									SELECT *
										FROM coberturas
										WHERE (LOWER(o.categoria) = 'Obra Social') AND (o.sigla = pp.sigla) AND
													(pp.sigla = coberturas.sigla) AND
													(h.sigla = o.sigla) AND
													(pp.nroplan = coberturas.nroplan)

							) AND (h.pagado = 'si')
		GROUP BY p.apellido, p.nombre, o.nombre, pp.nroplan
	) AS temp
WHERE (row_number <= 3)
