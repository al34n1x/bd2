/*19*/
create view vw_estudios_por_instituto as(
    select i.instituto as Instituto ,e.estudio as Estudio,h.fecha as Fecha, count (h.idestudio) as CantidadEstudio
        from institutos as i join historias as h on i.idinstituto=h.idinstituto
        join estudios as e on e.idestudio=h.idestudio
        where datediff(day,h.fecha,getdate()) between 0 and 7
        group by i.instituto, e.estudio  ,h.fecha
        )
