/*2*/

/*
Crear un procedimiento para ingresar estudios programados.
input: nombre del estudio, dni del paciente, matrícula del médico, nombre del instituto, sigla de la ooss, un entero que inserte la cantidad de estudios a realizarse, entero que indique el lapso en días en que los mismos deben realizarse.
¬	Generar todos los registros necesarios en la tabla historias.

*/

CREATE PROCEDURE ingresarEstudiosProgramados(
	@estudio varchar(100),
	@dni int,
	@matricula int,
	@instituto varchar(50),
	@sigla varchar(50),
	@cantidad int,
	@dias int)

	AS
		begin
			declare @proceder binary
			declare @counter int
			declare @fecha date
			declare @idinstituto int
			declare @idestudio int
			declare @observaciones varchar(50)
			declare @pagado varchar (2)
			set @counter = 0
			set @proceder = 1
			set @fecha = GETDATE()
			if not exists (select * from estudios where estudio = @estudio)
				begin
					print @estudio + ' no es un estudio valido'
					set @proceder = 0
				end
			if not exists (select * from pacientes where dni = @dni)
				begin
					print @dni + ' no pertenece a un paciente registrado'
					set @proceder = 0
				end
			if not exists (select * from medicos where matricula = @matricula)
				begin
					print @matricula + ' no pertenece a un medico registrado'
					set @proceder = 0
				end
			if not exists (select * from institutos where instituto = @instituto)
				begin
					print @instituto + ' no es un instituto registrado'
					set @proceder = 0
				end
			if not exists (select * from ooss where sigla = @sigla)
				begin
					print @sigla + ' no es una obra social / prepaga soportada'
					set @proceder = 0
				end
			if (@proceder = 1)
				begin
					while (@counter < @cantidad)
						begin
							set @fecha = DATEADD(d, @dias, @fecha)
							set @idinstituto = (select idinstituto from institutos where @instituto=instituto)
							set @idestudio = (select idestudio from estudios where @estudio = estudio)
							set @pagado = 'si'
							set @observaciones = 'Estudio Programado'
							insert into historias values (@dni, @idestudio, @fecha, @idinstituto, @matricula, @sigla, @pagado, @observaciones)
							set @counter = @counter + 1
						end
				end
		end
