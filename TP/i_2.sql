/*2*/

/*

Definir un Cursor que liste el detalle de los planes que cubren un determinado estudio identificando el porcentaje cubierto y la obra social, según formato:
Estudio.
Obra social.
Plan y Cobertura (ordenado en forma decreciente).


*/
CREATE PROCEDURE estudio_planes (@estudio nvarchar(50)) AS
BEGIN
	declare @idestudio int
	declare @sigla varchar(50)
	declare @nroplan int
	declare @cobertura int
	declare @OOSS varchar(50)
	declare @plan varchar(50)
	declare @OOSS_anterior varchar(50)
	declare @aux_idestudio int
	set @OOSS_anterior = null
	set @aux_idestudio = (select idestudio from estudios where estudio = @estudio)
	declare cursor_planes CURSOR FOR (
										SELECT
											c.sigla,
											c.nroplan,
											c.cobertura
										FROM coberturas c
										WHERE c.idestudio = @aux_idestudio
										)
	open cursor_planes
	fetch next from cursor_planes INTO @sigla, @nroplan, @cobertura
	print '////////////////////////////////////////////////////////'
	print 'Estudio: ' + @estudio
	while (@@FETCH_STATUS = 0)
	begin
		if ((@OOSS_anterior IS NULL) or (@OOSS_anterior <> @sigla))
		begin
			SELECT
				@OOSS = o.nombre
			FROM
				ooss o
			WHERE
				o.sigla = @sigla
			print 'Obra Social: ' + @OOSS + ' (' + @sigla + ')'
			set @OOSS_anterior = @sigla
		end
		SELECT
			@plan = p.nombre
		FROM
			planes p
		WHERE
			(p.sigla = @OOSS) AND
			(p.nroplan = @nroplan)
		print 'Plan: ' + CAST(@nroplan AS varchar(50)) + ' (' + @plan + ')'
		print 'Cobertura: ' + CAST(@cobertura AS varchar(50))
		fetch next from cursor_planes INTO @sigla, @nroplan, @cobertura
	end
	close cursor_planes
	deallocate cursor_planes
END
