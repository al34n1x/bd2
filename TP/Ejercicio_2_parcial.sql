/*2.	Vista y correlación.

Crear una vista VA999999 que muestre los platos con los 2(dos) precios más caros de cada rubro,
considere que puede haber dos o más platos con esos precios.
(No puede usar MAX o MIN, sub consultas en la cláusula from. Operadores no ANSI y debe correlacionar)

*/


CREATE VIEW VA999999
AS
	select e.estudio, p.precio
		from precios p join estudios e on p.idestudio = e.idestudio
		where 2 > (select count(*) from precios where p.precio < precio )
