/*6*/

/*1.	Crear un procedimiento que proyecte los datos de los médicos para una determinada especialidad.
input: nombre de la especialidad y sexo (default null).
¬	Proyectar los datos de los médicos activos que cumplan con la condición.
¬	Si no se especifica sexo, listar ambos.
*/
create procedure medicos_especialidad
    @especialidad varchar(30),
    @sexo varchar = null
      AS
        if @sexo is null
          begin
            select m.nombre, m.apellido, m.matricula, m.sexo
                    from medicos m inner join espemedi e on m.matricula = e.matricula
                    inner join especialidades ee on ee.idespecialidad = e.idespecialidad and ee.especialidad = @especialidad
                    where m.activo = 'si'
          end
        else
          begin
              select m.nombre, m. apellido, m.matricula, m.sexo
                from medicos m inner join espemedi e on m.matricula = e.matricula
                inner join especialidades ee on ee.idespecialidad = e.idespecialidad and ee.especialidad = @especialidad
                where m.activo = 'si' and m.sexo = @sexo
          end
  GO
