/*13*/
create view vw_estudios_sin_cobertura as (
    select c.cobertura as Cobertura, e.estudio as Estudio
    from coberturas as c right join estudios as e on c.idestudio = e.idestudio
    where c.cobertura is null
)

