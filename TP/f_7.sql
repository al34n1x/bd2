/*7*/

/*
Crear un procedimiento que proyecte los estudios que están cubiertos por una determinada obra social.
input: nombre de la ooss, nombre del plan ( default null ).
¬	Proyectar los estudios y la cobertura que poseen (estudio y porcentaje cubierto.
¬	Si no se ingresa plan, se deben listar todos los planes de la obra social.
*/

create procedure estudias_cubiertos_por_ooss
    @nombreOOSS varchar(30),
    @nombrePlan varchar (30) = null
    AS
      if @nombrePlan is null
          begin
            print @nombreOOSS
            select o.nombre,e.estudio,c.cobertura, p.nombre
              from ooss o inner join planes p on o.sigla = p.sigla
              inner join coberturas c on o.sigla = c.sigla
              inner join estudios e on e.idestudio = c.idestudio
              where o.nombre = @nombreOOSS
          end
      else
          begin
            select o.nombre,e.estudio,c.cobertura, p.nombre
              from ooss o inner join planes p on o.sigla = p.sigla
              inner join coberturas c on o.sigla = c.sigla
              inner join estudios e on e.idestudio = c.idestudio
              where o.nombre = @nombreOOSS and p.nombre = @nombrePlan
          end
GO
