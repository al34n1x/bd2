/* Primera Carga */

insert into medicos values('Juan','Perez','si','masculino',1000)

insert into especialidades values('Traumatologo')

insert into espemedi values (1000,1)

insert into estudios values( 'Control cadera','si')

insert into estuespe values (1,1)

insert into institutos values('San Juan De Dios','si')

insert into precios values (1,0,500)

insert into ooss values('OSDE','OSDE','Obra Social')

insert into planes values('OSDE',100,'Plan 210','si')

insert into pacientes values(23465879,'Sebastian','Carrizo','masculino','12/12/1985')

insert into afiliados values(23465879,'OSDE',100,23)

insert into coberturas values ('OSDE',100,1,50)

insert into historias values(23465879,1,'08/22/2015',0,1000,'OSDE','si','Completado')

/*ALTA OOSS*/

insert into ooss values('UP','Union Personal','Obra Social')
insert into ooss values('CEMIC','Centro de Educacion Medica e Inv. Clinicas','Obra Social')
insert into ooss values('SW','Swiss Medical','Prepaga')
insert into ooss values('GL','Galeno','Prepaga')
insert into ooss values('IO','IOMA','Obra Social')

/* ALTA PLANES */

insert into planes values('UP',200,'Acord 110','si')
insert into planes values('UP',201,'Acord 210','si')
insert into planes values('UP',202,'Acord 310','si')
insert into planes values('UP',203,'Plan dorado','no')


insert into planes values('OSDE',101,'Plan 310','si')
insert into planes values('OSDE',102,'Plan 410','si')
insert into planes values('OSDE',103,'Plan 450','no')
insert into planes values('OSDE',104,'Plan 510','si')

insert into planes values('SW',300,'SMG02','si')
insert into planes values('SW',301,'SMG10','si')
insert into planes values('SW',302,'SMG20','si')
insert into planes values('SW',303,'SMG30','si')

insert into planes values('GL',400,'Plan 220','si')
insert into planes values('GL',401,'Plan 330','si')
insert into planes values('GL',402,'Plan 440','si')
insert into planes values('GL',403,'Plan 550','no')

insert into planes values('IO',500,'Categoria A','si')
insert into planes values('IO',501,'Categoria B','si')
insert into planes values('IO',502,'Categoria C','si')

insert into planes values('CEMIC',600,'Plan 200 y Derivados','si')
insert into planes values('CEMIC',601,'Plan 300 y Derivados','si')
insert into planes values('CEMIC',602,'Plan 700 y Derivados','no')

/* ALTA MEDICOS */

insert into medicos values('Amelia','Perez','si','femenino',2000)
insert into medicos values('Julio','Sandoval','si','masculino',3000)
insert into medicos values('Federico','Juarez','si','masculino',4000)
insert into medicos values('Laura','Epuin','si','femenino',5000)
insert into medicos values('Carlos','Lauria','si','masculino',6000)
insert into medicos values('Mauro','Avellana','no','masculino',7000)
insert into medicos values('Sofia','Monteno','si','femenino',8000)

/* alta especialidades */

insert into especialidades values('Cirujano')
insert into especialidades values('Pediatra')
insert into especialidades values('Clinico')
insert into especialidades values('Neurologo')
insert into especialidades values('Dermatologo')
insert into especialidades values('Oftalmologo')
insert into especialidades values('Oseo')

/*ALTA INSTITUTOS*/

insert into institutos values('Insituto Medico Platense','si')
insert into institutos values('Insituto Medico Dolorense','si')
insert into institutos values('IPENSA','si')
insert into institutos values('TRINIDAD MEDICAL CENTER PALERMO','no')
insert into institutos values('Alfa centro medico','si')

/*ALTA PACIENTES*/

insert into pacientes values(26589634,'Maria','Robira','femenino','11/01/1990')
insert into pacientes values(35529734,'Mariana','Grebe','femenino','11/11/1991')
insert into pacientes values(36257416,'Adrian','Lopez','masculino','11/09/1992')
insert into pacientes values(20854297,'Claudio','Roldan','masculino','11/10/1965')
insert into pacientes values(32328962,'Agustina','Tormey','femenino','11/07/1987')
insert into pacientes values(35196875,'Luciana','Maxwell','femenino','11/02/1980')
insert into pacientes values(30613794,'Bruno','Valero','masculino','11/03/1980')
insert into pacientes values(25384699,'Alex','Castro','masculino','06/26/1975')

/*ALTA ESPEMEDI*/

insert into espemedi values (2000,2)
insert into espemedi values (3000,3)
insert into espemedi values (4000,4)
insert into espemedi values (5000,5)
insert into espemedi values (6000,6)
insert into espemedi values (6000,7)
insert into espemedi values (7000,7)

/*ALTA ESTUDIOS*/

insert into estudios values( 'Reduccion Cintura','si')
insert into estudios values( 'Laboratorio','si')
insert into estudios values( 'Examen Oftalmologico','si')
insert into estudios values( 'Rotura de ligamentos','si')
insert into estudios values( 'Traumatismo craneal','si')
insert into estudios values( 'Infeccion respiratoria','si')
insert into estudios values( 'Dermatocirugia','si')
insert into estudios values( 'Tumoracion Cutanea','si')

/*ALTA ESTUESPE*/

insert into estuespe values ( 2,2)
insert into estuespe values ( 3,3)
insert into estuespe values ( 4,4)
insert into estuespe values ( 5,5)
insert into estuespe values ( 6,6)
insert into estuespe values ( 7,7)
insert into estuespe values ( 2,3)
insert into estuespe values ( 5,7)
insert into estuespe values ( 6,2)

/*ALTA PRECIOS*/

insert into precios values (2,2,500)
insert into precios values (5,5,600)
insert into precios values (6,2,1500)
insert into precios values (1,3,90)
insert into precios values (7,4,300)
insert into precios values (9,4,750)

/*ALTA AFILIADOS*/

insert into afiliados values(26589634,'UP',200,550)
insert into afiliados values(35529734,'OSDE',101,22)
insert into afiliados values(36257416,'GL',401,600)
insert into afiliados values(20854297,'GL',402,321)
insert into afiliados values(32328962,'OSDE',102,485)
insert into afiliados values(35196875,'IO',501,1021)
insert into afiliados values(30613794,'SW',302,98)
insert into afiliados values(26589634,'GL',402,550)
insert into afiliados values(32328962,'SW',302,485)

/*ALTA COBERTURA*/

insert into coberturas values ('UP',200,2,20)
insert into coberturas values ('GL',401,2,22)
insert into coberturas values ('GL',402,3,30)
insert into coberturas values ('OSDE',102,4,60)
insert into coberturas values ('IO',501,5,51)
insert into coberturas values ('SW',302,6,10)

/*ALTA HISTORIAS*/

insert into historias values(35529734,2,'03/22/2016',2,1000,'OSDE','si','En proceso' )
insert into historias values(26589634,2,'09/08/2016',2,2000,'UP','si','En proceso' )
insert into historias values(36257416,5,'04/30/2016',5,3000,'GL','si','En proceso' )
insert into historias values(20854297,7,'11/11/2016',4,2000,'GL','si','En proceso' )
insert into historias values(32328962,5,'06/09/2016',5,4000,'OSDE','si','En proceso' )
insert into historias values(35196875,7,'03/01/2016',4,5000,'IO','si','En proceso' )
insert into historias values(30613794,1,'12/06/2016',3,5000,'SW','si','En proceso' )
insert into historias values(35196875,5,'09/11/2016', 5,5000,'IO','si','En proceso' )
insert into historias values(35196875,5,'12/11/2016', 5,5000,'IO','si','En proceso' )
insert into historias values(35196875,5,'12/12/2016', 5,5000,'IO','si','En proceso' )
insert into historias values(35196875,5,'09/28/2016', 5,5000,'IO','si','En proceso' )
insert into historias values(32328962,7,'09/24/2016',4,4000,'OSDE','si','En proceso' )
insert into historias values(35196875,7,'09/24/2016',4,5000,'IO','si','En proceso' )


