/*4*/
create view vw_pacientes_sin_cobertura as(
        select  p.dni as DNI, p.nombre as Nombre, p.apellido as Apellido, p.sexo as Sexo, p.nacimiento as FechaNac
            from pacientes as p left join afiliados as a on p.dni=a.dni
                where a.dni is null
            )
