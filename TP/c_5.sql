/*5*/
/* Hay que hacer un left join para que traiga los valores de los usuarios contra los logins, independientemente del valor null del login name*/

select users.name as name, logins.loginname as login
    from sys.sysusers as users
        left join sys.syslogins as logins on
            users.sid = logins.sid
