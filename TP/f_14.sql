/*14*/
/*

Crear un procedimiento que devuelva la cantidad posible de juntas médicas que puedan crearse combinando los médicos existentes.
input: / output: entero.
¬	Retornar la cantidad de combinaciones posibles de juntas entre médicos (2 a 6) que se pueden generar con los médicos activos de la Base de Datos.
Nota: Combinatoria (m médicos tomados de a n ) = m! / n! (m-n)! en una variable.


*/
CREATE PROCEDURE juntasMedicas(
	@n int,
	@combine bigint output)
	AS
	begin
		declare @m bigint
		set @m = (select COUNT(*) FROM medicos WHERE activo = 'si')
		SELECT dbo.Factorial(@m) / (dbo.Factorial(@n) * dbo.Factorial(@m - @n))
		set @combine = (dbo.Factorial(@m) / (dbo.Factorial(@n) * dbo.Factorial(@m - @n)))
	end
