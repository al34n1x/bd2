/*13B*/
CREATE TRIGGER delete_OOSS ON OOSS INSTEAD OF DELETE AS
	declare @OOSS varchar(50)
	declare @auxiliary binary
	SELECT
		@OOSS = sigla
	FROM
		deleted
	SELECT
		@auxiliary = planes.activo
	FROM
		planes
	WHERE
		planes.sigla = @OOSS
	if (@auxiliary = 0)
	begin
		DELETE FROM OOSS
		WHERE
			sigla = @OOSS
	end
	else
	begin
		UPDATE OOSS
		SET activo = 0
		WHERE
			sigla = @OOSS
		print 'La obra social selecionada tiene planes activos. No se puede borrar'
	end
