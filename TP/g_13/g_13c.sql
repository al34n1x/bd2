/*13C*/
CREATE TRIGGER delete_especialidad ON especialidades INSTEAD OF DELETE AS
	declare @especialidad int
	declare @auxiliary int
	SELECT
		@especialidad = idespecialidad
	FROM
		deleted
	SELECT
		@auxiliary = estuespe.idespecialidad
	FROM
		estuespe LEFT JOIN estudios
		ON
			(estuespe.idestudio = estudios.idestudio)
		LEFT JOIN espemedi
		ON
			(espemedi.idespecialidad = estuespe.idespecialidad)
		LEFT JOIN medicos
		ON
			(medicos.matricula = espemedi.matricula)
	WHERE
		(
			(estudios.activo = 1) OR
			(medicos.activo = 1)
		) AND
		(estuespe.idespecialidad = @especialidad)
	if (@auxiliary IS NULL)
	begin
		DELETE FROM especialidades
		WHERE
			idespecialidad = @especialidad
	end
	else
	begin
		UPDATE especialidades
		SET activo = 0
		WHERE
			idespecialidad = @especialidad
		print 'La especialidad no se puede eliminar ya que existen estudios que pueden ser recetados por la misma o m�dicos activos que ejercen la especialidad en cuesti�n.'
	end
