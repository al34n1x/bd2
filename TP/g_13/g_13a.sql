/*13A*/
CREATE TRIGGER delete_estudio ON estudios INSTEAD OF DELETE AS
	declare @estudio int
	declare @auxiliary int
	SELECT
		@estudio = idestudio
	FROM
		deleted
	SELECT
		@auxiliary = estudios.idestudio
	FROM
		estudios INNER JOIN estuespe
		ON
			(estudios.idestudio = estuespe.idestudio)
		INNER JOIN precios
		ON
			(estudios.idestudio = precios.idestudio)
		INNER JOIN historias
		ON
			(estudios.idestudio = historias.idestudio)
		INNER JOIN coberturas
		ON
			(estudios.idestudio = coberturas.idestudio)
	WHERE
		estudios.idestudio = @estudio
	if (@auxiliary IS NULL)
	begin
		DELETE FROM estudios
		WHERE
			idestudio = @estudio
	end
	else
	begin
		UPDATE estudios
		SET activo = 0
		WHERE
			idestudio = @estudio
		print 'El estudio seleccionado tiene informaci�n asociado. No se puede borrar'
	end
