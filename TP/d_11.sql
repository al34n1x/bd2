/*11*/

create view vw_pagos_pacientes as ( 
    select pa.nombre as Nombre, pa.dni as DNI, e.estudio as Estudio, h.fecha as Fecha, p.precio as Precio
    from historias as h join pacientes as pa on h.dni = pa.dni
    join precios as p on p.idestudio = h.idestudio
    join estudios as e on e.idestudio = p.idestudio
    group by pa.nombre, pa.dni, e.estudio, h.fecha, p.precio
)
