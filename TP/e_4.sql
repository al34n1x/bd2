/*4*/
/*Cuáles son los médico con mas especialidades y que atendieron
a pacientes que tengan una letra N en el apellido. */

SELECT m.nombre as NombreMedico, m.apellido as ApellidoMedico, m.activo as Activo, m.sexo as Sexo, m.matricula as Matricula
	FROM medicos m  RIGHT JOIN
	(
		SELECT *
			FROM
				(
				SELECT e.matricula, COUNT(e.matricula) AS 'cantidad'
					FROM espemedi e
					WHERE EXISTS(
												SELECT *
													FROM historias LEFT JOIN pacientes ON (historias.dni = pacientes.dni)
													WHERE (UPPER(pacientes.apellido) LIKE '%N%') AND (e.matricula = historias.matricula)
												)
				 GROUP BY e.matricula
				) AS temp

			WHERE (temp.cantidad = (
																SELECT MAX(cant_esp) AS 'maximo'
																	FROM (
																					SELECT espemedi.matricula, COUNT(espemedi.matricula) As 'cant_esp'
																						FROM espemedi
																						WHERE EXISTS(
																													SELECT *
																														FROM historias LEFT JOIN pacientes ON (historias.dni = pacientes.dni)
																														WHERE (UPPER(pacientes.apellido) LIKE '%N%') AND (espemedi.matricula = historias.matricula)
																												)
																						GROUP BY espemedi.matricula
																				) AS temp2
														)
				)
	) AS temp3 ON (m.matricula = temp3.matricula)
