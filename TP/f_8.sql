/*8*/
/*
Crear un procedimiento que proyecte cantidad de estudios realizados agrupados por ooss, nombre del plan y matricula del médico.
input: nombre de la ooss, nombre del plan, matrícula del médico.
¬	Proyectar la cantidad de estudios realizados.
¬	Si no se indica alguno de los parámetros se deben discriminar todas las ocurrencias.

*/


CREATE PROCEDURE estudiosOOSS (
	@OOSS varchar(50) = null,
	@nroPlan int = null,
	@matricula int = null)
	AS
		begin
			declare @sigla varchar (50)

			if ((@OOSS IS NULL) AND (@nroPlan IS NULL) AND (@matricula IS NULL))
				begin
					print 'Error, ingresar OOSS, NroPlan y Matricula'
			end

			else
				begin
					set @sigla = (select sigla from ooss where nombre = @ooss)
					SELECT m.apellido, m.nombre, o.nombre, a.nroplan, COUNT(h.dni)
						FROM historias h, medicos m, ooss o, afiliados a
						WHERE (h.dni = a.dni) AND
									(h.matricula = m.matricula) AND
									(h.sigla = o.sigla) AND
									(ISNULL(@OOSS, 0) = 0) AND
									(o.sigla = @sigla) AND
									(ISNULL(@nroPlan, 0) = 0) AND
									(a.nroplan = @nroPlan) AND
									(ISNULL(@matricula, 0) = 0) AND
									(m.matricula = @matricula)
						GROUP BY
							m.apellido,
							m.nombre,
							o.nombre,
							a.nroplan,
							h.dni
			end

		end

GO
