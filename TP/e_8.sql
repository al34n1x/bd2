/*8*/
/*Cuánto dinero implica los estudios ya realizados
por sus pacientes de aquellos pacientes que pertenezcan al médico con dinero recetado en estudios.
*/

SELECT m.apellido as ApellidoMedico, m.nombre as NombreMedico, SUM(temp.precio) AS 'Importe'
	FROM historias h LEFT JOIN medicos m ON (h.matricula = m.matricula)
	LEFT JOIN (
							SELECT *
								FROM precios
								WHERE EXISTS(
															SELECT *
																FROM historias
																WHERE (historias.pagado = 'si') AND
																			(precios.idestudio = historias.idestudio) AND
																			(precios.idinstituto = historias.idinstituto)
													  )
					) AS temp ON (h.idestudio = temp.idestudio) AND (h.idinstituto = temp.idinstituto)
	GROUP BY m.apellido, m.nombre
