/*1*/

/*
Definir una función que devuelva la edad de un paciente.
input: fecha de nacimiento.
output: edad expresada en años cumplidos.
*/

CREATE FUNCTION Edad(@FechaDeNacimiento Date) RETURNS int AS
begin
	declare @edad int
	set @edad = DATEDIFF(yy, @FechaDeNacimiento, GETDATE())
	return @edad
end
