/*2*/

/*
Definir las siguientes funciones para obtener, partiendo del nombre del estudio pasado como parámetro:
input: nombre del estudio.
output:	mayor precio del estudio.
menor precio del estudio.
precio promedio del estudio.

*/

/*Mayor*/
CREATE FUNCTION mayor_Precio(@estudio varchar(50)) RETURNS decimal AS
begin
	declare @mayor decimal
	declare @idestudio int
	set @idestudio = (select idestudio from estudios where estudio = @estudio)
	SELECT @mayor = MAX(p.precio)
		FROM precios p
		WHERE p.idestudio = @idestudio
	return @mayor
end

/*Menor*/
CREATE FUNCTION menor_Precio(@estudio varchar(50)) RETURNS decimal AS
begin
	declare @menor decimal
	declare @idestudio int
	set @idestudio = (select idestudio from estudios where estudio = @estudio)
	SELECT @menor = MIN(p.precio)
		FROM precios p
		WHERE p.idestudio = @idestudio
	return @menor
end

/*Promedio*/
CREATE FUNCTION Promedio(@estudio varchar(50)) RETURNS decimal AS
begin
	declare @promedio decimal
	declare @idestudio int
	set @idestudio = (select idestudio from estudios where estudio = @estudio)
	SELECT @promedio = AVG(p.precio)
	FROM precios p
	WHERE
		p.idestudio = @idestudio
	return @promedio
end
