/*15*/

create view vw_tabla_de_precios as (
    select e.estudio as NombreEstudio, o.nombre as NombreOOSS, p.nombre as NombrePlan, i.instituto as NombreInstituto, c.cobertura as PorcentajeCubierto, NetoFacturarPaciente = (pp.precio - (pp.precio * c.cobertura)/100), pp.precio as NetoFacturarOOSS  
    from coberturas as c join estudios as e on c.idestudio = e.idestudio
    join precios as pp on e.idestudio = pp.idestudio
    join ooss as o on c.sigla = o.sigla
    join planes as p on c.nroplan = p.nroplan
    join institutos as i on pp.idinstituto = i.idinstituto)
