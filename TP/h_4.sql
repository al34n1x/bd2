/*4*/

/*Definir una función que devuelva los n institutos más utilizados por especialidad.
input: nombre de la especialidad, cantidad máxima de institutos.
output: una Tabla de institutos (los n primeros ).
*/

CREATE FUNCTION rankInstitutosPorEspecialidad(@especialidad varchar(50), @cantidad int) RETURNS @table TABLE (especialidad varchar(50), instituto varchar(50), cantidad int) AS
	begin
	INSERT INTO @table (especialidad, instituto, cantidad)
	(
		SELECT temp.especialidad, temp.instituto, temp.cantidad
			FROM
				( SELECT ROW_NUMBER() OVER(ORDER BY COUNT(i.instituto) DESC) AS rownumber,
						e.especialidad, i.instituto, COUNT(i.instituto) AS 'cantidad'
						FROM historias h LEFT JOIN medicos m ON (h.matricula = m.matricula)
						LEFT JOIN espemedi e ON (m.matricula = e.matricula)
						LEFT JOIN institutos i ON (h.idinstituto = i.idinstituto)
						LEFT JOIN especialidades ee ON (e.idespecialidad = ee.idespecialidad)
						WHERE (ee.idespecialidad = dbo.getEspecialidad(@especialidad))
						GROUP BY ee.especialidad, i.instituto
				) AS temp
			WHERE (rownumber <= @cantidad)
	)
	return
end
