/* Creacion de tablas - PK no pueden ser null */
/* Debe de crearse primero todas las restricciones que tengan PK para luego referenciar las FK */

use db2989mg08


create table pacientes(
    dni int not null,
    nombre varchar (50),
    apellido varchar(50),
    sexo varchar(20),
    nacimiento datetime,
)

alter table pacientes
add constraint pk_dni primary key (dni),
    constraint ck_pacientes_sexo check (sexo = 'masculino' or sexo = 'femenino')
    go


    create table medicos(
            nombre varchar(50) ,
                apellido varchar(50) ,
                    activo varchar(2) ,
                        sexo varchar(20) ,
                            matricula int not null,
                        )

                        alter table medicos
                        add constraint pk_matricula primary key (matricula),
                            constraint ck_sexo check (sexo = 'masculino' or sexo = 'femenino'),
                                constraint ck_medico_activo check (activo = 'si' or activo = 'no')
                                go


                                create table especialidades(
                                        idespecialidad int not null identity (1,1),
                                            especialidad varchar(50),
                                        )

                                        alter table especialidades
                                        add constraint pk_idespecialidad primary key (idespecialidad)
                                        go

                                        create table estudios(
                                            idestudio int not null identity,
                                            estudio varchar(50),
                                            activo varchar(2),
                                        )

                                        alter table estudios
                                        add constraint pk_idestudio primary key (idestudio),
                                            constraint ck_estudios_activo check (activo = 'si' or activo = 'no')
                                            go

                                            create table ooss(
                                                sigla varchar(10) not null,
                                                nombre varchar(50),
                                                categoria varchar(20),
                                            )

                                            alter table ooss
                                            add constraint pk_sigla primary key (sigla),
                                                    constraint ck_ooss_categoria CHECK (categoria IN ('obra social', 'prepaga'))
                                                    go


                                                    create table espemedi(
                                                            matricula int not null,
                                                                idespecialidad int not null
                                                            )

                                                            alter table espemedi
                                                            add constraint pk_espemedi PRIMARY KEY (matricula, idespecialidad),
                                                                constraint fk_espemedi_matricula foreign key (matricula) references medicos,
                                                                    constraint fk_espemedi_idespecialidad foreign key (idespecialidad) references especialidades
                                                                    go

                                                                    create table estuespe(
                                                                            idestudio int not null,
                                                                                idespecialidad int not null
                                                                            )

                                                                            alter table estuespe
                                                                            add constraint pk_estuespe PRIMARY KEY (idestudio, idespecialidad),
                                                                                constraint fk_estuespe_idestudio foreign key (idestudio) references estudios,
                                                                                    constraint fk_estuespe_idespecialidad foreign key (idespecialidad) references especialidades
                                                                                    go

                                                                                    create table institutos(
                                                                                            idinstituto int IDENTITY(0, 1),
                                                                                                instituto varchar(50),
                                                                                                    activo varchar(2),
                                                                                                )

                                                                                                alter table institutos
                                                                                                add constraint pk_idinstituto primary key (idinstituto),
                                                                                                    constraint ck_institutos_activo check (activo = 'si' or activo = 'no')
                                                                                                    go

                                                                                                    create table precios(
                                                                                                        idestudio int not null,
                                                                                                        idinstituto int not null,
                                                                                                        precio money not null
                                                                                                    )

                                                                                                    alter table precios
                                                                                                    add constraint pk_precios PRIMARY KEY (idestudio, idinstituto),
                                                                                                        constraint fk_precios_idestudio foreign key (idestudio) references estudios,
                                                                                                            constraint fk_precios_idinstituto foreign key (idinstituto) references institutos
                                                                                                            go


                                                                                                            create table planes(
                                                                                                                sigla varchar(10) not null,
                                                                                                                nroplan int not null,
                                                                                                                nombre varchar(50),
                                                                                                                activo varchar(2)
                                                                                                            )

                                                                                                            alter table planes
                                                                                                            add constraint pk_planes PRIMARY KEY (sigla, nroplan),
                                                                                                                constraint fk_planes_sigla foreign key (sigla) references ooss,
                                                                                                                    constraint ck_planes_activo check (activo = 'si' or activo = 'no')

                                                                                                                    go


                                                                                                                    create table afiliados(
                                                                                                                        dni int not null,
                                                                                                                        sigla varchar(10) not null,
                                                                                                                        nroplan int not null,
                                                                                                                        nrofiliado int
                                                                                                                    )

                                                                                                                    alter table afiliados
                                                                                                                    add constraint pk_afiliados PRIMARY KEY (dni, sigla),
                                                                                                                        constraint fk_afiliados_pacientes FOREIGN KEY (dni) REFERENCES pacientes(dni),
                                                                                                                            constraint fk_afiliados_planes FOREIGN KEY (sigla, nroplan) REFERENCES planes(sigla, nroplan)
                                                                                                                            go

                                                                                                                            create table coberturas(
                                                                                                                                sigla varchar(10) not null,
                                                                                                                                nroplan int not null,
                                                                                                                                idestudio int not null,
                                                                                                                                cobertura int
                                                                                                                            )

                                                                                                                            alter table coberturas
                                                                                                                            add constraint pk_coberturas PRIMARY KEY (sigla, nroplan, idestudio),
                                                                                                                                constraint fk_coberturas_planes FOREIGN KEY (sigla, nroplan) REFERENCES planes(sigla, nroplan),
                                                                                                                                    constraint fk_coberturas_estudios FOREIGN KEY (idestudio) REFERENCES estudios(idestudio)
                                                                                                                                    go


                                                                                                                                    create table historias(
                                                                                                                                        dni int not null,
                                                                                                                                        idestudio int not null,
                                                                                                                                        fecha datetime not null,
                                                                                                                                        idinstituto int,
                                                                                                                                        matricula int,
                                                                                                                                        sigla varchar(10),
                                                                                                                                        pagado varchar (2),
                                                                                                                                        observaciones varchar(500),
                                                                                                                                    )


                                                                                                                                    alter table historias
                                                                                                                                    add constraint pk_historias PRIMARY KEY (dni, idestudio, fecha),
                                                                                                                                        constraint fk_historias_medicos FOREIGN KEY (matricula) REFERENCES medicos(matricula),
                                                                                                                                            constraint fk_historias_precios FOREIGN KEY (idestudio, idinstituto) REFERENCES precios(idestudio, idinstituto),
                                                                                                                                                constraint fk_historias_pacientes FOREIGN KEY (dni) REFERENCES pacientes(dni),
                                                                                                                                                    constraint fk_historias_afiliados FOREIGN KEY (dni, sigla) REFERENCES afiliados(dni, sigla),
                                                                                                                                                        constraint ck_historias_pagado check (pagado = 'si' or pagado = 'no')
                                                                                                                                                        go
