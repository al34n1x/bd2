/*15*/

/*

Crear un Trigger que determine el número de plan correspondiente al plan de
la obra social cada vez que se registre uno nuevo en la Base de Datos.
El número de plan se calcula en forma correlativa a los ya existentes para esa
obra social, comenzando con 1 (uno) el primer plan que se ingrese a cada obra social o prepaga.

*/

CREATE TRIGGER numerodeplan ON planes INSTEAD OF INSERT AS
	declare @nombre nvarchar(50)
	declare @ooss nvarchar(50)
	declare @numero int
	SELECT @ooss = sigla, @nombre = nombre
		FROM inserted
	SELECT @numero = COUNT(*) + 1
		FROM planes
		WHERE (sigla = @OOSS)
	INSERT INTO planes (sigla, nroplan, nombre, activo) VALUES (@ooss, @numero, @nombre, 'si')
