/*7*/

/*

Definir una función que devuelva los médicos que ordenaron repetir un mismo
estudio a un mismo paciente en los últimos n días.
input: cantidad de días.
output:  Tabla que proyecte el estudio repetido, nombre y fechas de
realización, identificación del paciente y del médico.

*/

CREATE FUNCTION Control_Estudios_Repetidos_Medico(@dias int)
RETURNS @table TABLE (
	medico varchar(100),
	paciente varchar(100),
	estudio varchar(50),
	cantidad int)
	AS
	begin
		INSERT INTO @table (medico, paciente, estudio, cantidad)
		(
			SELECT m.apellido + ', ' + m.nombre AS 'medico', p.apellido + ', ' + p.nombre AS 'paciente', e.estudio, COUNT(e.estudio) AS 'cantidad'
				FROM historias h LEFT JOIN medicos m ON (h.matricula = m.matricula)
				LEFT JOIN pacientes p ON (h.dni = p.dni)
				LEFT JOIN estudios e ON (h.idestudio = e.idestudio)
				WHERE (h.fecha >= DATEADD(d, -@dias, GETDATE()))
				GROUP BY m.apellido, m.nombre, p.apellido, p.nombre, e.estudio
	)
	return
end
