
/* Obtener path a base mediante SP_helpdb master */


/* Creacion Base */
create database db2989mg08
on
(
    name='db2989_data',
    filename='c:\Program Files\Microsoft SQL Server|MSSQL11.MSQLSERVER\MSSQL\DATA\db2989mg08.mdf',
    size=5MB,
    maxsize=50MB,
)
log on
(
    name='db2989_log',
    filename='c:\Program Files\Microsoft SQL Server|MSSQL11.MSQLSERVER\MSSQL\DATA\db2989mg08.ldf',
    size=2MB,
    maxsize=50MB,
)
go


/* Creacion Login */

create login db2989mg08
with
password = 'db2989mg08',
default_database = db2989mg08,
check_expiration = off,
check_policy = off,;
go


create login db2989mg08r
with
password = 'db2989mg08r',
default_database = db2989mg08,
check_expiration = off,
check_policy = off,;
go



create login db2989mg08w
with
password = 'db2989mg08r',
default_database = db2989mg08,
check_expiration = off,
check_policy = off,;
go

/* Cambiar Owner - recordar estar posicionado en la BD y no en la master */

use db2989mg08;
sp_changedbowner db2989mg08


/* Creacion Usuarios */

create user dbr for login db2989mg08r with default schema = db2989mg08;
create user dbw for login db2989mg08w with default schema = db2989mg08;


/* Creacion de roles */

sp_addrole 'db_lector'
sp_addrole 'db_grabador'

/* Agregar roles recien creados a los roles de sistema */

sp_addrolemember 'db_datawriter','db_grabador'
sp_addrolemember 'db_datareader','db_lector'

/* Agregar los usuarios dbw y dbr a los roles db_grabador y db_lector */

sp_addrolemember 'db_grabador','dbw'
sp_addrolemember 'db_lector','dbr'


/* Crear Tabla e Insert datos */

use db2989mg08;

create table Integrantes
(nroLibreta int not null,
    nombre varchar (40) not null,
    apellido varchar (40) not null,
    curso int not null,
    numeroDeGrupo int not null,

    primary key (nroLibreta));

