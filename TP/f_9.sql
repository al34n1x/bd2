/*9*/

/*
Crear un procedimiento que proyecte dni, fecha de nacimiento, nombre y apellido de los pacientes que correspondan a los n (valor solicitado) pacientes más viejos cuyo apellido cumpla con determinado patrón de caracteres.
input: cantidad (valor n), patrón caracteres (default null).
¬	Proyectar los pacientes que cumplan con la condición.

*/
create procedure pacientes_mas_viejos
    @cantidad int,
    @patron varchar(20) = null
    AS
      if @patron is null
        begin
          select dni, nombre, apellido, nacimiento
            from pacientes
            where @cantidad >(select count (*) from pacientes p where year (p.nacimiento) < year(pacientes.nacimiento))
        end
      else
        begin
          select dni, nombre, apellido, nacimiento
            from pacientes
            where apellido like @patron  and @cantidad >(select count (*) from pacientes p where year (p.nacimiento) < year(pacientes.nacimiento))
        end
GO
