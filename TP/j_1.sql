/*1*/

/*

1.	Definir una transacción para modificar la sigla y el nombre de una obra social que se inicie desde una stored procedure que recibe los parámetros de la obra social a modificarse.
¬	INPUT: sigla anterior, sigla nueva, nombre nuevo.
¬	RETURN: codigo de error.
¬	Se deben actualizar en cadena todas las tablas afectadas al proceso, en caso de error se anulará la transacción presentando el mensaje correspondiente y devolviendo un código de error.
¬	Modificar en caso de ser necesario la definición de los atributos de las tablas que impidan la ejecución de la transacción.


*/

CREATE PROCEDURE update_OOSS (
	@sigla_anterior nvarchar(50),
	@sigla_nueva nvarchar(50),
	@nombre nvarchar(50))
	AS
begin
	declare @presente nvarchar(50)
	declare @error int
	begin transaction
		SELECT
			@presente = OOSS.sigla
		FROM
			OOSS
		WHERE
			(OOSS.sigla = @sigla_anterior)
		if (@presente IS NULL)
		begin
			print 'Obra social ' + @sigla_anterior + ' no existe'
		end
		else
		begin
			UPDATE ooss SET sigla = @sigla_nueva, nombre = @nombre WHERE sigla = @sigla_anterior
			set @error = @@ERROR
			UPDATE historias SET sigla = @sigla_nueva WHERE sigla = @sigla_anterior
			set @error = @error + @@ERROR
			UPDATE planes SET sigla = @sigla_nueva WHERE sigla = @sigla_anterior
			set @error = @error + @@ERROR
			UPDATE afiliados SET sigla = @sigla_nueva WHERE sigla = @sigla_anterior
			set @error = @error + @@ERROR
			UPDATE coberturas SET sigla = @sigla_nueva WHERE sigla = @sigla_anterior
			set @error = @error + @@ERROR
			if (@error = 0)
			begin
				COMMIT
			end
			else
			begin
				ROLLBACK
			end
		end
end
