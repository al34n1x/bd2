/*3*/
create view vw_pacientes as(
    select p.dni as DNI, p.nombre as Nombre, p.apellido as Apellido, p.sexo as Sexo, p.nacimiento as FechaNac,a.sigla as Sigla,a.nroplan as NroPlan,a.nrofiliado as NroAfiliado
    from pacientes as p join afiliados as a on p.dni=a.dni
)
