/*20*/

/*
Crear un Trigger que controle que todas las historias que correspondan al
estudio que hace referencia en ese instituto, se encuentran pagadas para poder
permitir que se modifique el precio del estudio.
*/

CREATE TRIGGER control_Precios ON precios FOR UPDATE AS
	declare @estudio int
	declare @instituto int
	declare @precio decimal
	declare @auxiliar int

	SELECT @estudio = idestudio, @instituto = idinstituto, @precio = precio
		FROM deleted

	SELECT @auxiliar = h.dni
		FROM historias h
		WHERE (h.idestudio = @estudio) AND
					(h.idinstituto = @instituto) AND
					(h.pagado = 'si')
	if (@auxiliar IS NOT NULL)
		begin
			UPDATE precios SET precios.precio = @precio WHERE (precios.idestudio = @estudio) AND (precios.idinstituto = @instituto)
		print 'No se ha modificado el precio.'
	end
