/*3*/
/*
1.	Crear un procedimiento para ingresar datos del afiliado.
input: dni del paciente, sigla de la ooss, nro del plan, nro de afiliado.
¬	Si ya existe la tupla en Afiliados debe actualizar el nro de plan y el nro de afiliado.
¬	Si no existe debe crearla.


*/

create procedure ingresar_afiliado
    @dni int,
    @sigla varchar,
    @nroPlan int,
    @nroAfiliado int
      AS
        if exists(select dni from afiliados where dni = @dni and sigla = @sigla)
          begin
              update afiliados set nroplan = @nroPlan where dni = @dni and sigla = UPPER(@sigla)
              update afiliados set nrofiliado = @nroAfiliado where dni = @dni and sigla = UPPER(@sigla)
          end
        else
              insert into afiliados values (@dni,upper(@sigla),@nroPlan,@nroAfiliado)

GO
