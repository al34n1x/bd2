/*6*/

/*
Definir una función que devuelva los estudios y la cantidad de veces que se repitieron para
un mismo paciente a partir de una cantidad mínima que se especifique y dentro de un determinado período de tiempo.
input: cantidad mínima, fecha desde, fecha hasta.
output: una Tabla que proyecte el paciente, el estudio y la cantidad.

*/

CREATE FUNCTION estudios_Paciente(
	@minimo int,
	@desde Date,
	@hasta Date)
	RETURNS @table TABLE (
		apellido varchar(50),
		nombre varchar(50),
		estudio varchar(50),
		cantidad int)
	AS
	begin
		INSERT INTO @table (apellido, nombre, estudio, cantidad)
		(
			SELECT p.nombre, p.apellido, e.estudio, COUNT(h.dni) AS 'Cantidad'
				FROM historias h LEFT JOIN pacientes p ON (h.dni = p.dni)
				LEFT JOIN estudios e ON (h.idestudio = e.idestudio)
				WHERE (h.fecha >= @desde) AND (h.fecha <= @hasta)
				GROUP BY p.apellido, p.nombre, e.estudio
				HAVING COUNT(h.dni) >= @minimo
	)
	return
end
