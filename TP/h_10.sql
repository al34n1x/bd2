/*10*/

/*

Definir una función que devuelva la cantidad de estudios y la cantidad de
institutos para una determinada obra social.
input: sigla de la obra social.
output: una Tabla que proyecte obra social, estudio, cantidad del estudio, instituto, cantidad del instituto, (opcionalmente nro. de orden ).


*/

CREATE FUNCTION cant_estudios_institutos_ooss (@sigla varchar(50))
RETURNS @table TABLE (
	ooss varchar(50),
	instituto varchar(50),
	estudio varchar(50),
	cantestudio int)

AS
	begin
		INSERT INTO @table (ooss, instituto, estudio, cantestudio)
		(
			SELECT o.nombre, i.instituto, e.estudio, COUNT (h.dni) as 'Total_Pacientes'
			FROM historias h LEFT JOIN coberturas c ON (h.idestudio = c.idestudio)
				LEFT JOIN estudios e ON (c.idestudio = e.idestudio)
				LEFT JOIN precios p ON (e.idestudio = p.idestudio)
				LEFT JOIN institutos i  ON (p.idinstituto = i.idinstituto)
				LEFT JOIN OOSS o ON (c.sigla = o.sigla)
				WHERE c.sigla = @sigla
				GROUP BY o.nombre, e.estudio,i.instituto
		)
	return
end
