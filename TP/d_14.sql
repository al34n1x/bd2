/*14*/

create view vw_planes_sin_cobertura as (
    select o.nombre as NombreOOSS, p.nombre as NombrePlan, c.cobertura as Cobertura, c.idestudio as IDEstudio
    from coberturas as c right join planes as p on c.nroplan = p.nroplan
    join ooss as o on p.sigla = o.sigla
    where c.cobertura is null and c.nroplan is null)
