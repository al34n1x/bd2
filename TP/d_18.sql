/*18*/
create view vw_estudios_por_mes as(
      select datepart(month,h.fecha) as Mes, p.sexo as Sexo, e.estudio as Estudios, count (e.idestudio) as Cantidad
            from historias as h join estudios as e on h.idestudio = e.idestudio
            join pacientes as p on h.dni=p.dni
            group by datepart(month,h.fecha), p.sexo, e.estudio
            )
