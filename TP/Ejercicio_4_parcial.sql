/*Crear una función FB999999 que reciba como parámetro la descripción de un rubro y devuelva una tabla con todos los platos de ese rubro
excepto los de mayor precio y los de menor precio (si hubiese más de uno). Ejecutar la función y ordenar el resultado alfabéticamente.
(Utilice el operador ALL, no puede usar MIN o MAX y no puede utilizar sub consultas en la cláusula from, variables de tipo tabla y operadores no ANSI).
*/

CREATE FUNCTION FB999999 (@desc varchar(30))
  returns @resultado table (r_dec varchar (30), precio int)
  AS
  begin
    declare @e_idrubro int
    set @e_idrubro = (select idrubro from rubros where descripcion = @desc)
    insert @resultado
    select descripcion, precio
      from platos p join rubros r on p.idrubro = @e_idrubro
      where p.precio = ALL(select precio from platos where p.precio<platos.precio
      and platos.idrubro = @e_idrubro)) and precio NOT IN(select p.precio from platos p join rubros
      r on p.idrubro = @e_idrubro
      where p.precio = ALL (select precio from platos where p.precio > platos.precio and platos.idrubro = @e_idrubro))
      order by descripcion
    return

  end
