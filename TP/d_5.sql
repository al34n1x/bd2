/*5*/

create view vw_medicos_varias_especialidades as
(select m.nombre, m.matricula, count(m.matricula) as Cantidad
    from medicos as m join espemedi as e on m.matricula=e.matricula
    where m.activo ='si'
    group by m.nombre, m.matricula
    having count(m.matricula) > 1)
