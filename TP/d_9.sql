/*9*/

create view vw_cantidad_estudios_por_medico as (
    select m.matricula as Matricula, m.nombre as Nombre, e.estudio as Estudio, count (h.matricula) as Cantidad
        from estudios as e join historias as h on e.idestudio = h.idestudio
            join medicos as m on h.matricula = m.matricula
            group by h.matricula, e.estudio, m.matricula, m.nombre
        )
