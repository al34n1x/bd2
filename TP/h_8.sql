/*8*/

/*Definir una función que devuelva una cadena de caracteres en letras
minúsculas con la letra inicial de cada palabra en mayúscula.
input: string inicial.
output:  string convertido.
*/

CREATE FUNCTION string_convert(@input varchar(50)) 
RETURNS varchar(50)
AS
begin
	declare @resultado varchar(50)
	set @resultado = UPPER(SUBSTRING(@input, 1, 1))
	set @input = SUBSTRING(@input, 2, LEN(@input))
	while (LEN(@input) > 0)
	begin
		set @resultado = @resultado + LOWER(SUBSTRING(@input, 1, 1))
		/*Buscamos el espacio y de encontrarlo el siguiente caracter lo convertimos a mayuscula*/
		if (SUBSTRING(@input, 1, 1) = ' ')
			begin
				set @resultado = @resultado + UPPER(SUBSTRING(@input, 2, 1))
				set @input = SUBSTRING(@input, 3, LEN(@input))
			end
		else
			begin
				set @input = SUBSTRING(@input, 2, LEN(@input))
			end
	end
	return @resultado
end
