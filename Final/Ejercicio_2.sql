/*Ejercicio 2 Final*/


create procedure PA888888
	@input varchar(50),
	@output int output

	AS
		BEGIN
		declare @sentencia nvarchar(max)
		declare @e_idingrediente int
		declare @r_nombre varchar (50)
		declare @p_desc varchar (50)
		declare rubro cursor
		for	select r.descripcion, p.descripcion
				from rubros r join platos p on r.idrubro = p.idrubro
				join plaing pl on p.idplato = pl.idplato
				join ingredientes i on pl.idingrediente = i.idingrediente
				where i.descripcion = @input
				order by r.descripcion, p.descripcion

		set @e_idingrediente = (select idingrediente from ingredientes where descripcion = @input)
		set @output = (select count (*) from ingredientes join plaIng on ingredientes.idingrediente = plaing.idingrediente where ingredientes.idingrediente = @e_idingrediente)

		open rubro
		fetch rubro into @r_nombre, @p_desc
		while(@@FETCH_STATUS = 0)
			begin
				print 'RUBRO:' +@r_nombre
				print '			' + 'PLATO:' +@p_desc
				fetch rubro into @r_nombre, @p_desc
			end
		close rubro
		deallocate rubro
		print 'Total RUBRO:'
		Print @output
		END
	GO
