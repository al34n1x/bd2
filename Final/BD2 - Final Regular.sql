/*1. PROCEDIMIENTO CON SENTENCIAS DDL, DML, CORRELACI�N Y CURSORES*/

create procedure pa as
begin
--PARTE 1
	declare @state nvarchar(1000)

	if(exists(select * from sysobjects where name = 'faltas'))
	begin
		set @state = 'drop table faltas'
		print @state
		exec sp_executesql @state
	end

	else
	begin
		set @state = 'create table faltas (tipoDoc char(3) not null, nroDoc int not null, idPartido smallint not null, minuto int, tipo varchar(10))'
		print @state
		exec sp_executesql @state		
		set @state = 'alter table faltas add constraint pk_faltas primary key (tipoDoc, nroDoc)'
		print @state
		exec sp_executesql @state
		set @state = 'alter table faltas add constraint fk_faltas_jug foreign key (tipoDoc, nroDoc) references jugadores(Tipococ, Nrodoc)'
		print @state
		exec sp_executesql @state
		set @state = 'alter table faltas add constraint fk_faltas_partido foreign key (idPartido) references partidos(Id_Partido)'
		print @state
		exec sp_executesql @state
		set @state = 'alter table faltas add constraint ck_faltas_minuto check (minuto between 0 and 120)'
		print @state
		exec sp_executesql @state
		set @state = 'alter table faltas add constraint ck_faltas_tarjeta check (tipo = Amarilla or tipo = Roja)'
		print @state
		exec sp_executesql @state
	end

--PARTE 2
	declare cursorPartidos cursor for
	select
		Id_Partido,
		Id_ClubL,
		Id_ClubV
	from
		Partidos

	declare @idp int, @clubl tinyint, @clubv tinyint
	declare @tipod varchar(10), @nrod int

	open cursorPartidos
	fetch next from cursorPartidos into @idp, @clubl, @clubv
	while(@@FETCH_STATUS = 0)
	begin
		if(@idp % 2 = 0)
			select
				@tipod = Tipodoc,
				@nrod = Nrodoc
			from
				Jugadores j
			where
				j.Id_Club = @clubl
				and
				2 = (select COUNT(*) from Jugadores j1 where j1.Id_Club = j.Id_Club and j1.Nrodoc < j.Nrodoc)

		else
			select
				@tipod = Tipodoc,
				@nrod = Nrodoc
			from
				Jugadores j
			where
				j.Id_Club = @clubv
				and
				4 = (select COUNT(*) from Jugadores j1 where j1.Id_Club = j.Id_Club and j1.Nrodoc < j.Nrodoc)

		set @state = 'insert into faltas (tipoDoc, nroDoc, idPartido, minuto, tipo) ('+@tipod+','+CAST(@nrod as CHAR(10))+','+CAST(@idp as CHAR(3))+','+CAST(RAND(120) as CHAR(3))+',Amarilla)'
		print @state
		exec sp_executesql @state

		fetch next from cursorPartidos into @idp, @clubl, @clubv
	end

	close cursorPartidos
	deallocate cursorPartidos
end

--TEST:
exec pa
select * from faltas
drop procedure pa


/*2. TRIGGER*/

create trigger tr on faltas instead of insert as
begin
	declare @tipod char(10), @nrod int, @idp int, @min int, @tipo varchar(10)

	select
		@tipod = tipoDoc,
		@nrod = nroDoc,
		@idp = idPartido,
		@min = minuto,
		@tipo = tipo
	from
		deleted

	if(@tipo = 'Amarilla' or @tipo = 'Roja')
	begin
		if(not exists(select * from faltas where tipoDoc = @tipod and nroDoc = @nrod and idPartido = @idp and tipo = 'Roja'))
		begin
			insert into faltas values (@tipod, @nrod, @idp, @min, @tipo)
			print 'Se insert� una tarjeta ' + @tipo
		end
		
		else
			print 'No se pudo insertar ya que el jugador tiene una tarjeta roja'
	end

	if(@tipo = 'Roja')
	begin
		if(not exists(select * from faltas where tipoDoc = @tipod and nroDoc = @nrod and idPartido = @idp and tipo = 'Roja'))
		begin
			insert into faltas values (@tipod, @nrod, @idp, @min, @tipo)
			print 'Se insert� un nuevo registro'
		end
		
		else
			print 'Ya existe una tarjeta roja para el jugador'
	end

	if(@tipo = 'Amarilla')
	begin
		if(exists(select * from faltas where tipoDoc = @tipod and nroDoc = @nrod and idPartido = @idp and tipo = 'Amarilla'))
		begin
			insert into faltas values (@tipod, @nrod, @idp, @min, 'Roja')
			print 'Se cambi� la tarjeta amarilla por una roja'
		end
	end
end


/*3. PROCEDIMIENTO CON TRANSACCIONES, PAR�METROS DE OUTPUT Y SQL DIN�MICO*/

alter procedure pb (@cat int, @regs int output) as
begin
	declare @state nvarchar(1000)

	if(@cat = 84 or @cat = 85)
	begin
		if(exists(select * from sysobjects where name = 'resumen_' + CAST(@cat as char(2))))
		begin
			set @state = 'delete from resumen_' + CAST(@cat as char(2))
			print @state
			exec sp_executesql @state
		end
		
		else
		begin
			set @state = 'create table resumen_' + CAST(@cat as char(2)) + ' (idClub tinyint, 
																			mombreClub varchar(30), 
																			zona tinyint, 
																			puntosLocal int, 
																			puntosVisitante int, 
																			promedioLocal float(2), 
																			promedioVisitante float(2))'
			print @state
			exec sp_executesql @state
			set @state = 'alter table resumen_' + CAST(@cat as char(2)) + ' add constraint pk_resumen_' + CAST(@cat as char(2)) + ' primary key(idClub, zona)'
			print @state
			exec sp_executesql @state
			set @state = 'alter table resumen_' + CAST(@cat as char(2)) + ' add constraint fk_resumen_' + CAST(@cat as char(2)) + ' foreign key(idClub) references clubes(id_Club)'
			print @state
			exec sp_executesql @state
			set @state = 'alter table resumen_' + CAST(@cat as char(2)) + ' add constraint ck_resumen_' + CAST(@cat as char(2)) + '_ptsL check(puntosLocal > 0))'
			print @state
			exec sp_executesql @state
			set @state = 'alter table resumen_' + CAST(@cat as char(2)) + ' add constraint ck_resumen_' + CAST(@cat as char(2)) + '_ptsV check(puntosVisitante > 0))'
			print @state
			exec sp_executesql @state
			set @state = 'alter table resumen_' + CAST(@cat as char(2)) + ' add constraint ck_resumen_' + CAST(@cat as char(2)) + '_promL check(promedioLocal > 0))'
			print @state
			exec sp_executesql @state
			set @state = 'alter table resumen_' + CAST(@cat as char(2)) + ' add constraint ck_resumen_' + CAST(@cat as char(2)) + '_promV check(promedioVisitante > 0))'
			print @state
			exec sp_executesql @state
		end
		
		begin transaction tran1
		
		declare cursorClubes cursor for
		select
			Id_Club,
			Nombre,
			NroZona
		from
			Clubes
			
		declare @idc tinyint, @club varchar(30), @zona tinyint
		declare @cl tinyint, @cv tinyint, @gl tinyint, @gv tinyint
		declare @partidosL int, @partidosV int
		set @partidosL = 0
		set @partidosV = 0
		declare @ptsl int, @ptsv int
		
		open cursorClubes
		fetch next from cursorClubes into @idc, @club, @zona
		set @state = 'insert into resumen_' + CAST(@cat as char(2)) + '(idClub, nombreClub, zona) values ('+CAST(@idc as char(2))+','+@club+','+CAST(@zona as char(1))+')'
		print @state
		exec sp_executesql @state
		
		save transaction tran1
		
		while(@@FETCH_STATUS = 0)
		begin
			declare cursorPartidos cursor for
			select 
				Id_ClubL, 
				Id_ClubV, 
				GolesL, 
				GolesV
			from 
				Partidos
			where 
				Id_ClubL = @idc or Id_ClubV = @idc

			open cursorPartidos
			fetch next from cursorPartidos into @cl, @cv, @gl, @gv
			while(@@FETCH_STATUS = 0)
			begin
				if(@cl = @idc)
				begin
					if(@gl > @gv)
					begin
						set @state = 'update resumen_'+CAST(@cat as char(2))+' set puntosLocal = puntosLocal + 3 where idClub = '+CAST(@idc as char(2))
						print @state
						exec sp_executesql @state
					end
					if(@gl = @gv)
					begin
						set @state = 'update resumen_'+CAST(@cat as char(2))+' set puntosLocal = puntosLocal + 1 where idClub = '+CAST(@idc as char(2))
						print @state
						exec sp_executesql @state
					end
					set @partidosL = @partidosL + 1
				end
				
				if (@cv = @idc)
				begin
					if(@gv > @gl)
					begin
						set @state = 'update resumen_'+CAST(@cat as char(2))+' set puntosVisitante = puntosVisitante + 3 where idClub = '+CAST(@idc as char(2))
						print @state
						exec sp_executesql @state
					end
					if(@gv = @gl)
					begin
						set @state = 'update resumen_'+CAST(@cat as char(2))+' set puntosVisitante = puntosVisitante + 1 where idClub = '+CAST(@idc as char(2))
						print @state
						exec sp_executesql @state
					end
					set @partidosV = @partidosV + 1
				end
				
				set @regs = @regs + 1
				
				set @state = 'select '+CAST(@ptsl as char(2))+' = puntosLocal,'+CAST(@ptsv as char(2))+' = puntosVisitante from resumen_'+CAST(@cat as char(2))+' where idClub = '+CAST(@idc as char(2))
				print @state
				exec sp_executesql @state
				
				if(@ptsl = @ptsv)
					ROLLBACK
				
				fetch next from cursorPartidos into @cl, @cv, @gl, @gv
			end
			
			set @state = 'update resumen_'+CAST(@cat as char(2))+' set promedioLocal = puntosLocal/'+CAST(@partidosL as char(2))+' where idClub = '+CAST(@idc as char(2))
			print @state
			exec sp_executesql state 
			set @state = 'update resumen_'+CAST(@cat as char(2))+' set promedioVisitante = puntosVisitante/'+CAST(@partidosV as char(2))+' where idClub = '+CAST(@idc as char(2))
			print @state
			exec sp_executesql state 
			
			close cursorPartidos
			deallocate cursorPartidos
			
			set @partidosL = 0
			set @partidosV = 0
			
			fetch next from cursorClubes into @idc, @club, @zona
		end
		
		COMMIT
	end

	else
	begin
		print 'No es una categor�a v�lida'
		return
	end
end

--TEST:
declare @r int
set @r = 0
exec pb 84, @r
print CAST(@r as char(2))


/*4. FUNCI�N CON RETORNO DE TABLA Y CORRELACI�N*/

create function fa (@club varchar(30)) returns table
as
return (
		select 
			*
		from 
			jugadores j
		where
			j.Id_Club = @club
			and not
			j.Fecha_Nac <= ALL (select Fecha_Nac from Jugadores j1 where j1.Id_Club = j.Id_Club and j1.Fecha_Nac < j.Fecha_Nac)
			and not
			j.Fecha_Nac >= ALL (select Fecha_Nac from Jugadores j1 where j1.Id_Club = j.Id_Club and j1.Fecha_Nac > j.Fecha_Nac)
		)

--TEST:
select * from fa(1)