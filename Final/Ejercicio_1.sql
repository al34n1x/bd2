/*Ejercicio Final 1*/

select i.descripcion, i.precComp
	from ingredientes i join plaIng on i.idingrediente = plaIng.idingrediente
	join platos p on plaing.idplato = p.idplato
	join rubros r on r.idrubro = p.idrubro
	where i.precComp = ALL (select precComp
								from ingredientes join plaing ping on ingredientes.idingrediente = ping.idingrediente
								join platos on platos.idplato = ping.idplato
								join rubros on rubros.idrubro = platos.idrubro
								where precComp<i.precComp and rubros.descripcion like '%carne%')
	group by i.descripcion, i.precComp
